'''
Created on 23.04.2015

@author: Michael Kamp
'''

import sys
import numpy as np
from types import IntType

class Kernel():
    def __init__(self, **kwargs):
        self.identifier = "Generic kernel class (please replace)"
        
    def __str__(self):
        return self.identifier
    
    def computeKernelValue(self, x1, x2):
        return 0.0
    
    def k(self, idx1, idx2, v, data):
        assert type(idx1) is IntType
        assert type(idx2) is IntType
        if self in data.kernels:
            if v in data.kernels[self]:
                return data.kernels[self][v][idx1,idx2]
        else:
            return self.getKernelValue(idx1, idx2, v, data)
    
    def computeKernelMatrix(self, X1, X2):
        K = np.zeros((len(X1), len(X2)))
        for i in xrange(len(X1)):
            for j in xrange(len(X2)):
                K[i][j] = self.computeKernelValue(X1[i], X2[j])
        return K        
    
    def K(self, X1idx, X2idx, vidx, data):                
        if data.hasPrecomputedKernel(self, vidx):
            try:
                K = data.getPrecomputedKernel(self,vidx)[X1idx][:,X2idx]
            except:
                print "error: ", sys.exc_info()[0]
                print X1idx
                print X2idx
                return np.array([[]])		  
            return K
        else:
            return self.getKernelMatrix(X1idx, X2idx, vidx, data)
    
    def getKernelValue(self, idx1, idx2, v, data):
        return self.computeKernelValue(data.getX(idx1, v), data.getX(idx1, v))
                    
    def getKernelMatrix(self, X1idx, X2idx, v, data):
        K = np.zeros((len(X1idx), len(X2idx)))
        for i in xrange(len(X1idx)):
            for j in xrange(len(X2idx)):
                K[i][j] = self.computeKernelValue(data.getX(X1idx[i],v), data.getX(X2idx[j], v))
        return K
    
#     def getKs(self, data, views, Xidx, X2idx = None):
#         Ks = []
#         for v in views:
#             if X2idx == None:
#                 X2idx = Xidx 
#             K = self.K(Xidx, X2idx, v, data)
#             Ks.append(K)
#         return np.array(Ks)
    
    def getK(self, data, vidx, Xidx, X2idx = None):
        if X2idx == None:
            X2idx = Xidx 
        return self.K(Xidx, X2idx, vidx, data)
    
    def precomputeKernel(self, data, vidx=None):
        if self not in data.kernels:
            data.kernels[self] = {}        
        views = data.views
        viewIdxs = []
        if vidx == None:
            viewIdxs = range(len(views))
        else:
            viewIdxs = [vidx]
        for vidx in viewIdxs:           
            if not data.hasPrecomputedKernel(self, vidx):
                K = self.K(data.getXidx(),data.getXidx(),vidx,data)
                v = tuple(data.views[vidx].tolist())    
                data.kernels[self][v] = K        
    
    def setParams(self, **kwargs):
        for param in kwargs:
            if hasattr(self, param):
                setattr(self, param, kwargs[param])
    
    def __eq__(self, other):        
        return isinstance(other, self.__class__)
    
    def __hash__(self):
        return (hash(self.identifier) ^ hash(self.__class__))
        
class LinearKernel(Kernel):
    def __init__(self, **kwargs):
        Kernel.__init__(self, **kwargs)
        self.identifier = "linear"
        
    def computeKernelValue(self, x1, x2):
        return np.dot(x1, x2)
    
    def computeKernelMatrix(self, X1, X2):
        Klin = np.dot(X1, X2.T)
        return Klin
    
    def getKernelMatrix(self, X1idx, X2idx, vidx, data):
        return self.computeKernelMatrix(data.getX(X1idx, vidx), data.getX(X2idx, vidx))
    
    def __eq__(self, other):        
        if isinstance(other, self.__class__):
            return True
        else:
            return False
    
    def __hash__(self):
        return (hash(self.identifier) ^ hash(self.__class__))   
    
class BiasedLinearKernel(Kernel):
    def __init__(self, **kwargs):
        Kernel.__init__(self, **kwargs)
        self.identifier = "bias_linear"
        
    def computeKernelValue(self, x1, x2):
        bias = 1.
        return np.dot(x1, x2) + bias
    
    def computeKernelMatrix(self, X1, X2):
        bias = 1.
        Klin = np.dot(X1, X2.T)
        Klin = Klin + bias*np.ones((len(X1), len(X2)))
        return Klin
    
    def getKernelMatrix(self, X1idx, X2idx, vidx, data):
        return self.computeKernelMatrix(data.getX(X1idx, vidx), data.getX(X2idx, vidx))
    
    def __eq__(self, other):        
        if isinstance(other, self.__class__):
            return True
        else:
            return False
    
    def __hash__(self):
        return (hash(self.identifier) ^ hash(self.__class__))        
    
class GaussKernel(Kernel):
    def __init__(self, **kwargs):
        Kernel.__init__(self, **kwargs)
        self.identifier = "rbf"
        if 'sigma' not in kwargs:
            self.sigma = None
        else:
            self.sigma = kwargs['sigma']
        
    def setSigma(self, sigma):
        self.sigma = sigma
        
    def computeKernelValue(self, x1, x2): #heuristic if sigma == None
        subst = np.subtract(x1,x2)
        val = np.dot(subst.T, subst)
        sigma = self.sigma
        if sigma == None:
            sigma = 1. / len(x1)
        return np.e**((-1.*val) / (sigma))   
    
    def computeKernelMatrix(self, X1, X2):
        sigma = self.sigma
        if sigma == None:
            sigma = 1. / len(X1[0])     
        D1 = np.diag(np.dot(X1, X1.transpose()))
        D2 = np.diag(np.dot(X2, X2.transpose()))
        X1X2 = np.dot(X1, X2.transpose())
        diffMatrix = D2[:,np.newaxis] + (D1[:,np.newaxis] + (-2)*X1X2).transpose() 
        Kgauss = np.exp((-1./float(sigma))*diffMatrix)  
        #print "sigma", sigma
        #print "Kgauss", Kgauss
        return Kgauss.T #not necessarily squared matrices
    
    def getKernelMatrix(self, X1idx, X2idx, vidx, data):                        
        X1 = data.getX(X1idx, vidx)
        X2 = data.getX(X2idx, vidx)
        return self.computeKernelMatrix(X1,X2)
    
    def __eq__(self, other):        
        if isinstance(other, self.__class__):
            return self.sigma == other.sigma
        else:
            return False
    
    def __hash__(self):
        return (hash(self.identifier) ^ hash(self.__class__) ^ hash(self.sigma))
    
class BiasedGaussKernel(Kernel):
    def __init__(self, **kwargs):
        Kernel.__init__(self, **kwargs)
        self.identifier = "bias_rbf"
        if 'sigma' not in kwargs:
            self.sigma = None
        else:
            self.sigma = kwargs['sigma']
        
    def setSigma(self, sigma):
        self.sigma = sigma
        
    def computeKernelValue(self, x1, x2): #heuristic if sigma == None
        bias = .1
        subst = np.subtract(x1,x2)
        val = np.dot(subst.T, subst)
        sigma = self.sigma
        if sigma == None:
            sigma = 1. / len(x1)
        return np.e**((-1.*val) / (sigma)) + bias    
    
    
    def computeKernelMatrix(self, X1, X2):
        bias = .1
        sigma = self.sigma
        if sigma == None:
            sigma = 1. / len(X1[0])     
        D1 = np.diag(np.dot(X1, X1.transpose()))
        D2 = np.diag(np.dot(X2, X2.transpose()))
        X1X2 = np.dot(X1, X2.transpose())
        diffMatrix = D2[:,np.newaxis] + (D1[:,np.newaxis] + (-2)*X1X2).transpose() 
        Kgauss = np.exp((-1./float(sigma))*diffMatrix)  
        Kgauss = Kgauss + bias*np.ones((len(X2), len(X1)))
        return Kgauss.T #not necessarily squared matrices
    
    def getKernelMatrix(self, X1idx, X2idx, vidx, data):                        
        X1 = data.getX(X1idx, vidx)
        X2 = data.getX(X2idx, vidx)
        return self.computeKernelMatrix(X1,X2)
    
    def __eq__(self, other):        
        if isinstance(other, self.__class__):
            return self.sigma == other.sigma
        else:
            return False
    
    def __hash__(self):
        return (hash(self.identifier) ^ hash(self.__class__) ^ hash(self.sigma))    
    
if __name__ == '__main__':
    kernel = GaussKernel(sigma = 1.)
    X = np.array([[1,1,1],[1,1,2],[1,1,1]])
    Y = np.array([[1,1,1],[1,1,2]])
    kVal = kernel.computeKernelMatrix(X,Y)
    Ktrue = np.array([[kernel.computeKernelValue(X[0],Y[0]),kernel.computeKernelValue(X[0],Y[1])],[kernel.computeKernelValue(X[1],Y[0]),kernel.computeKernelValue(X[1],Y[1])],[kernel.computeKernelValue(X[2],Y[0]),kernel.computeKernelValue(X[2],Y[1])]])

    
    
        


    
