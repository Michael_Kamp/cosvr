'''
Created on 16.04.2015

@author: Michael Kamp
'''

import numpy as np
from datetime import datetime
import itertools
import copy
import collections as cl
from kernel import GaussKernel, LinearKernel
from paramHeuristics import NoDataDependentFactor

class Learner:
    def __init__(self, paramRange = None, paramFactorHeuristic = NoDataDependentFactor(), **kwargs):  
        self.configureParams([],[],[]) 
        self.paramRange = self.getVerifiedParamRanges(paramRange)        
        self.views = []        
        self.paramFactHeuristic = paramFactorHeuristic
        self.checkKernel = False
        self.identifier = "GenericLearner (please replace)"
        
    def train(self, L, U, data):
        return True
    
    def predict(self, U, data):
        return np.zeros(U.shape[0])
        
    def __str__(self):
        return self.identifier
    
    def getNumberOfSupportVectors(self):
        if hasattr(self, 'svs'):
            numSVs = 0
            for viewSVs in self.svs:
                numSVs += len(viewSVs)
            return numSVs
        else:
            return 0
    
    def configureParams(self, viewIndependentParams, viewDependentParams, kernelDependentParams):
        if not hasattr(self, 'viewIndependentParams'):    self.viewIndependentParams    = viewIndependentParams
        if not hasattr(self, 'viewDependentParams'):      self.viewDependentParams      = viewDependentParams
        if not hasattr(self, 'kernelDependentParams'):    self.kernelDependentParams    = kernelDependentParams  
    
    def getVerifiedParamRanges(self, origParamRanges):
        standardParamRanges = self.getStandardParamRanges()
        paramRange = cl.defaultdict(list)
        for param in self.viewDependentParams:
            if param in origParamRanges:
                paramRange[param] = origParamRanges[param]
            else:
                paramRange[param] = standardParamRanges[param]
        for param in self.viewIndependentParams:
            if param in origParamRanges:
                paramRange[param] = origParamRanges[param]
            else:
                paramRange[param] = standardParamRanges[param]
        for param in self.kernelDependentParams:
            if param in origParamRanges:
                paramRange[param] = origParamRanges[param]
            else:
                paramRange[param] = standardParamRanges[param]
        return paramRange
        
    
    def getStandardParamRanges(self):
        self.standardParamRanges = cl.defaultdict(list)
        self.standardParamRanges['k'] = [GaussKernel()]
        self.standardParamRanges['nu'] = [1.]#[100.,10.,1.,.1,.01]
        #self.standardParamRanges['eps'] = [1.,.1]#[10.,1.,.5,.3]
        self.standardParamRanges['epsU'] = [1.,.1]#[10.,1.,.5,.3]
        self.standardParamRanges['epsL'] = [1.,.1]#[10.,1.,.5,.3]
        self.standardParamRanges['lmbda'] = [.1]#[.1,.001,.00001]
        self.standardParamRanges['sigma'] = [1., 10000., 100000000.]#[10.] #relatively high values necessary such that kernel entries are in a nice range
        return self.standardParamRanges     
    
    def paramValDataDependentFactor(self, param, data, vidx = None):
        return self.paramFactHeuristic.paramValDataDependentFactor(param, data, vidx) 
    
    def getParamVal(self, param, data, vidx=None):
        if not hasattr(self, param):
            return None
        paramVal = copy.deepcopy(getattr(self, param))
        if paramVal == None:
            return None
        if hasattr(paramVal, '__len__'):
            if vidx == None:
                for i in xrange(len(paramVal)):
                    if paramVal[i] != None:
                        paramVal[i] = paramVal[i]#*self.paramValDataDependentFactor(param, data, i)
            else:
                paramVal = paramVal[vidx]#*self.paramValDataDependentFactor(param, data, vidx)
        else: 
            #paramVal *= self.paramValDataDependentFactor(param, data) #when no view is given, the factor is calculated on all views
            pass
        return paramVal
    
    #structure of possParams is: list of possible parameter combinations. Each element is a list of views. Each element is a dict of params
    def getPossParams(self, numberOfViews):
        v = numberOfViews
        possParamCounts = {}
        paramRange = copy.deepcopy(self.paramRange)
        actParamRange = {}      
        if paramRange == None:
            print "shit"  
        for param in paramRange:
            if param in self.viewIndependentParams:
                possParamCounts[param] = len(paramRange[param])  
                actParamRange[param] = paramRange[param]  
            elif param in self.viewDependentParams:
                possParamCounts[param] = len(paramRange[param])**v
                actParamRange[param] = [np.array(list(combi)) for combi in itertools.product(paramRange[param], repeat = v)]
            elif param in self.kernelDependentParams and hasattr(self.k, param): #only check a kernel parameter if it belongs to the actual kernel
                possParamCounts[param] = len(paramRange[param])**v
                actParamRange[param] = [np.array(list(combi)) for combi in itertools.product(paramRange[param], repeat = v)]
        paramNames = sorted(actParamRange)
        possParams = [dict(zip(paramNames, prod)) for prod in itertools.product(*(actParamRange[varName] for varName in paramNames))]
        return possParams 
    
    def setParams(self, params):
        for param in params:
            if hasattr(self, param):
                setattr(self, param, params[param])
            else:
                #print "Warning: ",param," not a parameter for method ",self
                pass
    
    def processParameter(self, kwargs):
        for param in self.viewIndependentParams:
            if param in kwargs:
                setattr(self,param,kwargs[param])
            else:
                val = self.paramRange[param][0] #if no param is given, take the first element of the (standard) parameter range
                setattr(self,param,val)
        for param in self.viewDependentParams:
            if param in kwargs:
                setattr(self,param,kwargs[param])
            else:
                val = self.paramRange[param][0] #if no param is given, take the first element of the (standard) parameter range
                setattr(self,param,val)   
        for param in self.kernelDependentParams:
            if self.kernelRequiresParam(param):
                if param in kwargs:
                    setattr(self,param,kwargs[param])
                else:
                    val = self.paramRange[param][0] #if no param is given, take the first element of the (standard) parameter range
                    setattr(self,param,val)        
        
    
    def kernelRequiresParam(self, param):
        if hasattr(self, 'k'):
            k = self.k
        else:
            k = self.paramRange['k'][0]
        if hasattr(k, param):
            return True
        return False
            
    
    def getParamsAsString(self):
        params = "k: "+str(self.k)+" "
        if isinstance(self.k, GaussKernel):
            params += ", sigma: "+str(self.sigma)
        for param in self.viewDependentParams:
            params += str(param)+": "+str(getattr(self,param)) + " "
        for param in self.viewIndependentParams:
            if param != 'k':
                params += str(param)+": "+str(getattr(self,param)) + " "
        params += "views: " +str(self.views)
        return params
    
    def validateKernel(self, K):
        U = np.ones(K.shape)
        diffU = np.linalg.norm(U - K)
        I = np.identity(min(K.shape))
        diffI = abs(np.linalg.norm(K) - np.linalg.norm(I))
        norm = np.linalg.norm(K)
        thetaU = 2.0
        thetaI = 2.0
        thetaN = 2.0
        stOk = ""
        if diffU < thetaU:
            stOk += "U;"
        if diffI < thetaI:
            stOk += "I;"
        if norm < thetaN:
            stOk += "N;"       
        if stOk != "":
            print "\n*************************************"
            print "Kernel validation failed:"
            print "Difference from ones: \t\t", diffU, ",\t threshold = ", thetaU
            print "Difference to unit matrix: \t", diffI, ",\t threshold = ", thetaI
            print "Norm of kernel matrix: \t\t", norm, ",\t threshold = ", thetaN
            print K 
            print "*************************************"
        
        