'''
Created on 17.09.2015

@author: mkamp
'''

import numpy as np
    
class  NoDataDependentFactor():
    def paramValDataDependentFactor(self, param, data, vidx):
        return 1.
    
class CoSVR_Heuristic():
    def paramValDataDependentFactor(self, param, data, vidx):
        if param == 'epsL':
            return np.std(data.getYL())
        if param == 'epsU':
            return np.std(data.getYL())        
        if param == 'nu':
            #v = data.views[vidx]
            X = data.getL(vidx = vidx)
            fact = 1/float(len(X)) * np.sum((np.linalg.norm(X, axis=-1))) #not squared!
            return 1./fact
        if param == 'sigma':
            X = data.getL(vidx = vidx)
            XXT = np.dot(X, X.transpose())
            D = np.diag(XXT).reshape(-1,1)
            diffSquaredMatrix = D + (D -2*XXT).transpose()
            fact = (1./(float(len(X)))**2) * np.sum(np.sum(diffSquaredMatrix, axis=0).reshape(-1), axis=0)  
            return fact #inverse or not?????
        if param == 'lmbda':
            X = data.getU(vidx = vidx)
            fact = (1./float(len(X)**2)) * np.sum((np.linalg.norm(X, axis=-1))**2)
            return 1./fact
  
        
class CoRLSR_PaperHeuristic():
    def paramValDataDependentFactor(self, param, data, vidx):
        if param == 'epsL':
            return np.std(data.getYL())
        if param == 'epsU':
            return np.std(data.getYL())        
        if param == 'nu':
            #v = data.views[vidx]
            X = data.getL(vidx = vidx)
            fact = 1/float(len(X)) * np.sum((np.linalg.norm(X, axis=-1))) #not squared!
            return 1./fact
        if param == 'sigma':
            X = data.getL(vidx = vidx) #calculated only on labeled data
            XXT = np.dot(X, X.transpose())
            D = np.diag(XXT).reshape(-1,1)
            diffSquaredMatrix = D + (D -2*XXT).transpose()
            fact = (1./(float(len(X)))**2) * np.sum(np.sum(diffSquaredMatrix, axis=0).reshape(-1), axis=0)  
            #print "\npre-factor", fact
            return fact
        if param == 'lmbda':
            return .1