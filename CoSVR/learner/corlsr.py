import numpy as np
from numpy import linalg
from learner import Learner
#from kernel import GaussKernel, LinearKernel
#import collections as cl

class CoRLSR(Learner):
    '''Initializes the CoSVR. 
    Parameters:
    possParams    -    dictionary. For every parameter (key) it holds a list of possible parameters for parameter evaluation. 
    k             -    kernel
    nu            -    regularization parameter of the support vector regression.
    lmbda         -    parameter defining influence of co-regularization.    
    sigma         -    kernel width for Gaussian kernel
    '''      
    def __init__(self, **kwargs):        
        self.configureParams(viewIndependentParams = ['k','lmbda'], viewDependentParams = ['nu'], kernelDependentParams = ['sigma'])         
        self.svs = {}
        self.coeffs = {}
        Learner.__init__(self, **kwargs)     
        self.processParameter(kwargs)        
        self.identifier = "CoRLSR"     
        
    
    def train(self, L, U, data):
        self.views = data.views
        for param in self.viewDependentParams:
            if len(getattr(self,param)) == 1 and len(self.views) > 1:
                setattr(self,param,np.array(getattr(self,param)*len(self.views)))
        for param in self.kernelDependentParams:
            if self.kernelRequiresParam(param):
                if len(getattr(self,param)) == 1 and len(self.views) > 1:
                    setattr(self,param,np.array(getattr(self,param)*len(self.views)))        
        Ks = []
        Xidx = np.hstack((L,U))
        for v in self.views:
            vidx = data.getViewIndex(v)
            if hasattr(self, 'sigma'):
                self.k.setParams(sigma = self.getParamVal('sigma', data, vidx))
            self.k.precomputeKernel(data, vidx)            
            K = self.k.getK(data, vidx, Xidx)
            if self.checkKernel:
                self.validateKernel(K)
            Ks.append(K)        
#         try:
        ######I included
        #for param in self.viewIndependentParams:
        #    if param != 'k':
        #        kwargs[param] = self.getParamVal(param, data)
        #for param in self.viewDependentParams:
        #    kwargs[param] = self.getParamVal(param, data)  
        nu = self.getParamVal('nu', data)
        lmbda = self.getParamVal('lmbda', data)
        ######
        SVs, Coeffs = corlsr_train(Ks, Xidx, data.getYL(), nu, lmbda)
#         except:
#             SVs, Coeffs = [[]],[[]]
#             print self.nu, self.lmbda
        self.svs = SVs
        self.coeffs = Coeffs    
        return len(self.svs) > 0        
            
    def predict(self, Xidx, data):
        Ks = []
        for v in self.views:
            vidx = data.getViewIndex(v)
            if not hasattr(self.svs, '__len__') or len(self.svs) == 0:
                print "Warning: #SVs is 0 for view",vidx,
                return np.zeros(len(Xidx))   
            if hasattr(self, 'sigma'):
                self.k.setParams(sigma = self.getParamVal('sigma', data, vidx))        
            K = self.k.getK(data, vidx, Xidx, self.svs[vidx])
            if self.checkKernel:
                self.validateKernel(K)     
            Ks.append(K)                     
        y_pred = corlsr_predict(Ks, self.coeffs)
        return np.array(y_pred)

def corlsr_train(Ks, Xidx, y, nus, lam):
    n = len(y)
    nPm = len(Ks[0])
    #m = nPm - n
    M = len(Ks)

    A = np.zeros((M*nPm, M*nPm))
    b = np.zeros(M*nPm)
    for v in xrange(M):
        #print "view", v
        #print Ks[v]
        #print "n", n
        #print "Ks[0]", np.shape(Ks[0]), "Ks[1]", np.shape(Ks[1])
        #print "Ks", np.shape(Ks[v][:n].transpose())
        #print "y", np.shape(y.reshape(-1,1))
        b[v*nPm: (v+1)*nPm] = np.dot(Ks[v][:n].transpose(), y.reshape(-1,1)).reshape(-1)
        Gv = nus[v]*Ks[v] + np.dot(Ks[v][:n].transpose(), Ks[v][:n]) + 2*lam*(M-1)*np.dot(Ks[v][n:].transpose(), Ks[v][n:])      
        #Gv = nus*Ks[v] + np.dot(Ks[v][:n].transpose(), Ks[v][:n]) + 2*lam*(M-1)*np.dot(Ks[v][n:].transpose(), Ks[v][n:])
        A[v*nPm: (v+1)*nPm, v*nPm: (v+1)*nPm] = Gv
        for u in xrange(M):
            if u != v:
                UvTUu = -2*lam*np.dot(Ks[v][n:].transpose(), Ks[u][n:])
                A[v*nPm: (v+1)*nPm, u*nPm: (u+1)*nPm] = UvTUu
                A[u*nPm: (u+1)*nPm, v*nPm: (v+1)*nPm] = UvTUu.transpose()
    #coeffs = linalg.solve(A,b)
    #svs = range(M*nPm)
    A += 0.000001*np.identity(M*nPm)
    sol = linalg.solve(A,b)
    coeffs = []
    svs = []
    for v in xrange(M):
        coeffs.append(np.array(sol[v*nPm:(v+1)*nPm]))
        svs.append(np.array(Xidx))
    #svs and coeffs now list of lists
    return svs, coeffs

def corlsr_predict(Ks, coeffs):
    #Ks ... list of matrices
    #coeffs ... list of coefficient lists
    M = len(Ks)
    m = len(Ks[0])
    pred = np.zeros(m)
    for v in xrange(M):
        pred += 1/float(M)*np.dot(Ks[v], coeffs[v].reshape(-1,1)).reshape(-1)
    return pred.reshape(-1)
