import numpy as np
#from cvxopt import matrix, solvers
import math
from numpy import random
from rlsr import rlsr_train
from corlsr import corlsr_train
from svr import svr_train
from cosvr import eps_cosvr_train, l2_cosvr_train, sigma_cosvr_train
from sklearn.svm.classes import NuSVC
from scipy import stats
import matplotlib.pyplot as pl
from data.dataHandler import TOYDATA

def example1():
    ###toy example 1 ########
    #########################
    ###example parameters####
    lab_fraction = 0.1 
    nPm = 10
    nu = 1.0
    eps = 0.001
    lam = 1000.
    #########################
    n = int(nPm*lab_fraction)
    m = nPm-n
    X1 = np.concatenate((np.array(range(1,nPm+1)).reshape((-1,1)), np.array(range(1,nPm+1)).reshape((-1,1))), axis=1) #identical views
    X2 = np.concatenate((np.array(range(1,nPm+1)).reshape((-1,1)), np.array(range(1,nPm+1)).reshape((-1,1))), axis=1) 
    X1[n:,0] = np.sign(random.normal(0.0,10.0,m)) #Bernoulli
    X2[n:,1] = np.sign(random.normal(0.0,10.0,m)) 
    y, y_unlab = np.array(range(1,nPm+1))[:n], np.array(range(1,nPm+1))[n:]
    Xidx = range(nPm) #instance indices
    K1 = np.dot(X1, X1.transpose()) #linear kernel
    K2 = np.dot(X2, X2.transpose())
    svs1, coeffs1 = svr_train(K1[:n,:n], Xidx, y, nu, eps) #nu, eps
    svs2, coeffs2 = svr_train(K2[:n,:n], Xidx, y, nu, eps)
    svs_co, coeffs_co = eps_cosvr_train([K1,K2], Xidx, y, eps, eps, [nu, nu], lam) #epsL, epsU, nus, lam
    linmodel1 = np.dot(coeffs1.reshape((1,-1)), X1[svs1,:])
    linmodel2 = np.dot(coeffs2.reshape((1,-1)), X2[svs2,:])
    linmodel1_cosvr = np.dot(coeffs_co[0].reshape((1,-1)), X1[svs_co[0],:])
    linmodel2_cosvr = np.dot(coeffs_co[1].reshape((1,-1)), X2[svs_co[1],:])
    pred1 = np.dot(K1[n:,svs1], coeffs1.reshape((-1,1))).reshape(-1)
    pred2 = np.dot(K2[n:,svs2], coeffs2.reshape((-1,1))).reshape(-1)
    pred_cosvr = 0.5*(np.dot(K1[n:,svs_co[0]], coeffs_co[0].reshape((-1,1))).reshape(-1) + np.dot(K2[n:,svs_co[1]], coeffs_co[1].reshape((-1,1))).reshape(-1) )
    rmse1 = 1/float(math.sqrt(m))*np.linalg.norm(pred1.reshape(-1)-y_unlab.reshape(-1))  
    rmse2 = 1/float(math.sqrt(m))*np.linalg.norm(pred2.reshape(-1)-y_unlab.reshape(-1))  
    rmse_cosvr = 1/float(math.sqrt(m))*np.linalg.norm(pred_cosvr.reshape(-1)-y_unlab.reshape(-1))  
    
    print "----------------------------"
    print "rmse svr 1:", rmse1
    print "rmse svr 2:", rmse2
    print "rmse cosvr:", rmse_cosvr
    print "----------------------------"
    print "true labels:", y_unlab
    print "pred svr 1:", pred1
    print "pred svr 2:", pred2
    print "pred cosvr:", pred_cosvr
    print "----------------------------"
    print "linmodel svr 1:", linmodel1.reshape(-1)
    print "linmodel svr 2:", linmodel2.reshape(-1)
    print "linmodel 1 cosvr", linmodel1_cosvr.reshape(-1)
    print "linmodel 2 cosvr:", linmodel2_cosvr.reshape(-1)
    print "----------------------------"
    
def example2():
    ###toy example 2 ########
    #########################
    ###example parameters####
    lab_fraction = 0.1
    nPm = 100
    n = int(nPm*lab_fraction)
    m = nPm-n
    nu = 1
    eps = 0.001
    epsL = 0.001
    epsU = 0.001
    nus = [1, 1]
    lam = 10.
    nosd = 2 #number of deficient side diagonals
    norc = 2 #number of relevant columns
    r = random.normal(0.0,10.0,nPm) #random variables for columns
    #r = stats.bernoulli.rvs(0.2, size = nPm)
    #print "r", r
    #########################
    X = np.zeros((nPm,0))
    for _ in xrange(nPm+norc): #nPm+1 equal columns r as 
        X = np.concatenate((X, r.reshape((-1,1))), axis=1)
    X1 = np.copy(X)
    X2 = np.copy(X)
    X1[:,:] = random.normal(0.0,0.01,(nPm,nPm + norc))
    X2[:,:] = random.normal(0.0,0.01,(nPm,nPm + norc))
    """
    X1[n:nPm, :nPm] = np.zeros((m, nPm))
    X2[n:nPm, :nPm] = 0.01*np.ones((m, nPm)) #works!!
    #X2[n:nPm, :nPm] = np.zeros((m, nPm)) #does not work!!
    #print X1
    """
    """
    X1[range(nPm), range(nPm)] = 100
    X2[range(nPm), range(nPm)] = -100
    for i in xrange(1,nosd+1): #i is distance to main diagonal
        sidediagRange = range(i,nPm) + range(0,i)
        X1[sidediagRange, range(nPm)] = 100
        X1[range(nPm), sidediagRange] = 100
        X2[sidediagRange, range(nPm)] = -100
        X2[range(nPm), sidediagRange] = -100
    """
    #"""
    X1[range(nPm), range(nPm)] = np.zeros(nPm)
    X2[range(nPm), range(nPm)] = 0.01*np.ones(nPm)
    for i in xrange(1,nosd+1): #i is distance to main diagonal
        sidediagRange = range(i,nPm) + range(0,i)
        X1[sidediagRange, range(nPm)] = np.zeros(nPm)
        X1[range(nPm), sidediagRange] = np.zeros(nPm)
        X2[sidediagRange, range(nPm)] = np.zeros(nPm)
        X2[range(nPm), sidediagRange] = np.zeros(nPm)   
    #"""
    y,y_unlab = np.copy(r)[:n], np.copy(r)[n:]
    """
    perfModel = np.append(np.zeros(nPm), np.ones(1)).reshape((1,-1)) #bugfix approach
    X1 = np.concatenate((X1, perfModel), axis=0)
    X2 = np.concatenate((X2, perfModel), axis=0)
    y,y_unlab = np.copy(r)[:n], np.concatenate((np.copy(r)[n:], np.ones(1))) #with perfModel approach
    """
    Xidx = range(nPm+norc) #feature indices
    K1 = np.dot(X1, X1.transpose()) #linear kernel
    K2 = np.dot(X2, X2.transpose())
    
    ####SVR####
    svs1, coeffs1 = svr_train(K1[:n,:n], Xidx, y, nu, eps) #nu, eps
    svs2, coeffs2 = svr_train(K2[:n,:n], Xidx, y, nu, eps)
    svs_cosvr, coeffs_cosvr = eps_cosvr_train([K1,K2], Xidx, y, epsL, epsU, nus, lam) #epsL, epsU, nus, lambda

    linmodel1 = np.dot(coeffs1.reshape((1,-1)), X1[svs1,:])
    linmodel2 = np.dot(coeffs2.reshape((1,-1)), X2[svs2,:])
    linmodel1_cosvr = np.dot(coeffs_cosvr[0].reshape((1,-1)), X1[svs_cosvr[0],:])
    linmodel2_cosvr = np.dot(coeffs_cosvr[1].reshape((1,-1)), X2[svs_cosvr[1],:])

    pred1 = np.dot(K1[n:,svs1], coeffs1.reshape((-1,1))).reshape(-1)
    pred2 = np.dot(K2[n:,svs2], coeffs2.reshape((-1,1))).reshape(-1)
    pred_cosvr = 0.5*(np.dot(K1[n:,svs_cosvr[0]], coeffs_cosvr[0].reshape((-1,1))).reshape(-1) 
                      + np.dot(K2[n:,svs_cosvr[1]], coeffs_cosvr[1].reshape((-1,1))).reshape(-1) )

    rmse1 = 1/float(math.sqrt(m))*np.linalg.norm(pred1.reshape(-1)-y_unlab.reshape(-1))  
    rmse2 = 1/float(math.sqrt(m))*np.linalg.norm(pred2.reshape(-1)-y_unlab.reshape(-1))  
    rmse_cosvr = 1/float(math.sqrt(m))*np.linalg.norm(pred_cosvr.reshape(-1)-y_unlab.reshape(-1))  

    print "----------------------------"
    #print "true labels:", y_unlab
    #print "pred svr 1:", pred1
    #print "pred svr 2:", pred2
    #print "pred cosvr:", pred_cosvr
    #print "----------------------------"   
    #print "linmodel svr 1:", linmodel1.reshape(-1)#, np.linalg.norm(linmodel1) #norm for perfModel approach
    #print "linmodel svr 2:", linmodel2.reshape(-1)#, np.linalg.norm(linmodel2)
    #print "linmodel 1 cosvr", linmodel1_cosvr.reshape(-1)#, np.linalg.norm(linmodel1_cosvr)
    #print "linmodel 2 cosvr:", linmodel2_cosvr.reshape(-1)#, np.linalg.norm(linmodel2_cosvr)
    #print "----------------------------"  
    print "rmse svr 1:", rmse1
    print "rmse svr 2:", rmse2
    print "rmse cosvr:", rmse_cosvr
    print "----------------------------"   
    
    """
    ####RLSR####
    svs1rlsr, coeffs1rlsr = rlsr_train(K1[:n,:n], Xidx[:n], y, nu) #nu
    svs2rlsr, coeffs2rlsr = rlsr_train(K2[:n,:n], Xidx[:n], y, nu)
    svs_corlsr, coeffs_corlsr = corlsr_train([K1,K2], Xidx, y, nus, lam)
    
    #linmodel1_rlsr = np.dot(coeffs1rlsr.reshape((1,-1)), X1[svs1rlsr,:])
    #linmodel2_rlsr = np.dot(coeffs2rlsr.reshape((1,-1)), X2[svs2rlsr,:])
    #linmodel1_corlsr = np.dot(coeffs_corlsr[0].reshape((1,-1)), X1[svs_corlsr[0],:])
    #linmodel2_corlsr = np.dot(coeffs_corlsr[1].reshape((1,-1)), X2[svs_corlsr[1],:])
    
    pred1rlsr = np.dot(K1[n:,svs1rlsr], coeffs1rlsr.reshape((-1,1))).reshape(-1)
    pred2rlsr = np.dot(K2[n:,svs2rlsr], coeffs2rlsr.reshape((-1,1))).reshape(-1)
    pred_corlsr = 0.5*(np.dot(K1[n:,svs_corlsr[0]], coeffs_corlsr[0].reshape((-1,1))).reshape(-1) 
                      + np.dot(K2[n:,svs_corlsr[1]], coeffs_corlsr[1].reshape((-1,1))).reshape(-1) )
    
    rmse1rlsr = 1/float(math.sqrt(m))*np.linalg.norm(pred1rlsr.reshape(-1)-y_unlab.reshape(-1))  
    rmse2rlsr = 1/float(math.sqrt(m))*np.linalg.norm(pred2rlsr.reshape(-1)-y_unlab.reshape(-1))  
    rmse_corlsr = 1/float(math.sqrt(m))*np.linalg.norm(pred_corlsr.reshape(-1)-y_unlab.reshape(-1))  
    
    print "----------------------------"   
    print "rmse1rlsr:", rmse1rlsr
    print "rmse2rlsr:", rmse2rlsr
    print "rmse_corlsr:", rmse_corlsr
    print "----------------------------"   
    #print "linmodel1_rlsr:", linmodel1_rlsr, np.linalg.norm(linmodel1_rlsr)
    #print "linmodel2_rlsr:", linmodel2_rlsr, np.linalg.norm(linmodel2_rlsr)
    #print "linmodel1_corlsr", linmodel1_corlsr, np.linalg.norm(linmodel1_corlsr)
    #print "linmodel2_corlsr:", linmodel2_corlsr, np.linalg.norm(linmodel2_corlsr)
    print "----------------------------"   
    """
    
def example3():
    nPm = 200
    d = 100
    lab_fraction = 0.1
    #nPm = 200
    #d = 1000
    #lab_fraction = 0.05
    n = int(nPm*lab_fraction)
    m = nPm-n
    norf = 10 #number of relevant features
    sf = 1. #scaling factor for relevant features in view 2
    pos = 0.1 #probability of success
    
    nu = 1.
    eps = 0.01
    epsL = 0.01
    epsU = 0.01
    nus = [1., 1.]
    lam = 1.
    X1 = stats.bernoulli.rvs(pos, size = (nPm, d))
    X2 = stats.bernoulli.rvs(pos, size = (nPm, d))
    #X2 = np.copy(X1)
    RFC = stats.bernoulli.rvs(pos, size = (nPm, norf)) #relevant feature columns
    X1 = np.concatenate((X1, RFC), axis = 1)
    X2 = np.concatenate((X2, RFC), axis = 1)
    #for i in xrange(1,sf): #???
    #    X2 = np.concatenate((X2, RFC), axis = 1)
    Y = np.sum(RFC, axis = 1)
    y, y_unlab = Y[:n], Y[n:]
    Xidx = range(nPm)
    K1 = np.dot(X1, X1.transpose()) #linear kernel
    K2 = np.dot(X2, X2.transpose())
    
    ####SVR####
    svs1, coeffs1 = svr_train(K1[:n,:n], Xidx, y, nu, eps) #nu, eps
    svs2, coeffs2 = svr_train(K2[:n,:n], Xidx, y, nu, eps)
    svs_cosvr, coeffs_cosvr = eps_cosvr_train([K1,K2], Xidx, y, epsL, epsU, nus, lam) #epsL, epsU, nus, lambda

    linmodel1 = np.dot(coeffs1.reshape((1,-1)), X1[svs1,:])
    linmodel2 = np.dot(coeffs2.reshape((1,-1)), X2[svs2,:])
    linmodel1_cosvr = np.dot(coeffs_cosvr[0].reshape((1,-1)), X1[svs_cosvr[0],:])
    linmodel2_cosvr = np.dot(coeffs_cosvr[1].reshape((1,-1)), X2[svs_cosvr[1],:])

    pred1 = np.dot(K1[n:,svs1], coeffs1.reshape((-1,1))).reshape(-1)
    pred2 = np.dot(K2[n:,svs2], coeffs2.reshape((-1,1))).reshape(-1)
    pred_cosvr = 0.5*(np.dot(K1[n:,svs_cosvr[0]], coeffs_cosvr[0].reshape((-1,1))).reshape(-1) 
                      + np.dot(K2[n:,svs_cosvr[1]], coeffs_cosvr[1].reshape((-1,1))).reshape(-1) )

    rmse1 = 1/float(math.sqrt(m))*np.linalg.norm(pred1.reshape(-1)-y_unlab.reshape(-1))  
    rmse2 = 1/float(math.sqrt(m))*np.linalg.norm(pred2.reshape(-1)-y_unlab.reshape(-1))  
    rmse_cosvr = 1/float(math.sqrt(m))*np.linalg.norm(pred_cosvr.reshape(-1)-y_unlab.reshape(-1))  

    print "----------------------------"
    #print "true labels:", y_unlab
    #print "pred svr 1:", pred1
    #print "pred svr 2:", pred2
    #print "pred cosvr:", pred_cosvr
    #print "----------------------------"   
    print "linmodel svr 1:", linmodel1.reshape(-1)#, np.linalg.norm(linmodel1) #norm for perfModel approach
    print "linmodel svr 2:", linmodel2.reshape(-1)#, np.linalg.norm(linmodel2)
    print "linmodel 1 cosvr", linmodel1_cosvr.reshape(-1)#, np.linalg.norm(linmodel1_cosvr)
    print "linmodel 2 cosvr:", linmodel2_cosvr.reshape(-1)#, np.linalg.norm(linmodel2_cosvr)
    #print "----------------------------"  
    print "rmse svr 1:", rmse1
    print "rmse svr 2:", rmse2
    print "rmse cosvr:", rmse_cosvr
    print "----------------------------"   

def include(dataset, views, lab_fraction):
    path = '/home/katrin/iaisvn/camlstudents/KatrinMichael/CoSVR/data/LIGANDS/' 
    sparsitiesList = []
    for i in [0,1]:
        inFile = open(path + 'csv/' + views[i] + '/' + dataset + '_' + views[i] + '.csv', 'r').readlines()
        inFile.pop(0)
        if i == 0: #only once
            noe = len(inFile) #number of exmples
            Y = np.zeros(noe)
        dov = 0 #dimension of view
        xIndices = []
        yIndices = []
        for line, l in zip(inFile, xrange(noe)):
            numbersInLine = line.split(',') #position numbers in csv-file
            if i == 0: Y[l] = float(numbersInLine[-1]) #only once
            onesInLine = np.array(map(int, numbersInLine[:-1]))-1 #positions of ones, index shift
            lineMax = np.max(onesInLine) + 1 #absolute length
            if lineMax > dov: dov = lineMax
            noOnesInLine = len(onesInLine) #number of ones in line
            xIndices = xIndices + noOnesInLine*[l]
            yIndices = yIndices + list(onesInLine)
        dataMatrix = np.zeros((noe, dov))
        dataMatrix[xIndices, yIndices] = 1
        #sparsity = np.sum(dataMatrix)/float(noe*dov)
        #setInfo = np.array([noe, dov, sparsity]) #n.o.examples, dimension of view, ratio of zeros
        sparsity = np.sum(dataMatrix, axis=0)*1/float(noe)
        sparsitySortedIndices = np.argsort(sparsity) 
        sparsity.sort()
        sparsitiesList.append(sparsity)
        if i == 0:
            X1 = dataMatrix[:, list(sparsitySortedIndices)]
        elif i == 1:
            X2 = dataMatrix[:, list(sparsitySortedIndices)]
        n = int(noe*lab_fraction)    
        m = noe-n
    return X1, X2, Y, sparsitiesList, n, m, noe

def example4():
    lab_fraction = 0.1
    nu = 1.
    eps = 0.01
    epsL = 0.01
    epsU = 0.01
    nus = [1., 1.]
    lam = 1.
    datatype = 'real' #alternatives: 'real', 'toy'
    if datatype == 'toy':
        """
        TOYDATA
        """
        nPm = 200
        d = 100
        norf = 10 #n.o.relevant features
    
        n = int(nPm*lab_fraction)
        m = nPm-n
        dilationFactor = math.log(10**4)/float(d) #results in sparsities [1.0, ..., 10**-4]
        sparsities = [math.exp(-x*dilationFactor) for x in xrange(d)]
        #sparsities = [0.1 for x in xrange(d)]
        #fig, ax = pl.subplots()
        #pl.xlim(0.0,d)
        #pl.ylim(0.0,1.0)
        #ax.plot(range(d), sparsities, 'ro') #inverse sparsityDist
        #pl.show()
        
        order = np.arange(d) #dimension indices
        random.shuffle(order) #shuffled dimension indices
        #order = np.arange(d-norf,d)
        #print order
        rfs = order[:norf] #choice of dimension indices
        rfs.sort()
        RFC = np.zeros((nPm, 0)) #relevant feature columns
        
        X1 = np.zeros((nPm,0))
        X2 = np.zeros((nPm,0))
        for i in xrange(d):
            column1 = stats.bernoulli.rvs(sparsities[i], size=(nPm,1))
            X1 = np.concatenate((X1, column1), axis=1)
            if i in rfs: #relevant feature index
                RFC = np.concatenate((RFC, column1), axis=1)
                X2 = np.concatenate((X2, column1), axis=1)
            else:
                column2 = stats.bernoulli.rvs(sparsities[i], size=(nPm,1))
                X2 = np.concatenate((X2, column2), axis=1)
        
        Y = np.sum(RFC, axis = 1)
        #print "labels", Y

    elif datatype == 'real':
        """
        LIGAND DATA
        """
        dataset = 'P23946'
        views = ['ecfp4', 'maccs']
        X1, X2, Y, sparsitiesList, n, m, nPm = include(dataset, views, lab_fraction)
    
    y, y_unlab = Y[:n], Y[n:]
    Xidx = range(nPm)
    K1 = np.dot(X1, X1.transpose())+1 #linear kernel
    K2 = np.dot(X2, X2.transpose())+1
    
    ####SVR and COSVR####
    svs1, coeffs1 = svr_train(K1[:n,:n], Xidx, y, nu, eps) #nu, eps
    svs2, coeffs2 = svr_train(K2[:n,:n], Xidx, y, nu, eps)
    svs_cosvr, coeffs_cosvr = eps_cosvr_train([K1,K2], Xidx, y, epsL, epsU, nus, lam) #epsL, epsU, nus, lambda

    linmodel1 = np.dot(coeffs1.reshape((1,-1)), X1[svs1,:])
    linmodel2 = np.dot(coeffs2.reshape((1,-1)), X2[svs2,:])
    linmodel1_cosvr = np.dot(coeffs_cosvr[0].reshape((1,-1)), X1[svs_cosvr[0],:])
    linmodel2_cosvr = np.dot(coeffs_cosvr[1].reshape((1,-1)), X2[svs_cosvr[1],:])

    pred1 = np.dot(K1[n:,svs1], coeffs1.reshape((-1,1))).reshape(-1)
    pred2 = np.dot(K2[n:,svs2], coeffs2.reshape((-1,1))).reshape(-1)
    pred_cosvr = 0.5*(np.dot(K1[n:,svs_cosvr[0]], coeffs_cosvr[0].reshape((-1,1))).reshape(-1) 
                      + np.dot(K2[n:,svs_cosvr[1]], coeffs_cosvr[1].reshape((-1,1))).reshape(-1) )

    rmse1 = 1/float(math.sqrt(m))*np.linalg.norm(pred1.reshape(-1)-y_unlab.reshape(-1))  
    rmse2 = 1/float(math.sqrt(m))*np.linalg.norm(pred2.reshape(-1)-y_unlab.reshape(-1))  
    rmse_cosvr = 1/float(math.sqrt(m))*np.linalg.norm(pred_cosvr.reshape(-1)-y_unlab.reshape(-1))  

    


    print "----------------------------"
    #print "true labels:", y_unlab
    #print "pred svr 1:", pred1
    #print "pred svr 2:", pred2
    #print "pred cosvr:", pred_cosvr
    #print "----------------------------"   
    print "linmodel svr 1:", linmodel1.reshape(-1)#, np.linalg.norm(linmodel1) #norm for perfModel approach
    print "linmodel svr 2:", linmodel2.reshape(-1)#, np.linalg.norm(linmodel2)
    print "linmodel 1 cosvr", linmodel1_cosvr.reshape(-1)#, np.linalg.norm(linmodel1_cosvr)
    print "linmodel 2 cosvr:", linmodel2_cosvr.reshape(-1)#, np.linalg.norm(linmodel2_cosvr)
    #print "----------------------------"  
    print "rmse svr 1:", rmse1
    print "rmse svr 2:", rmse2
    print "rmse cosvr:", rmse_cosvr
    print "----------------------------"       
    
    print "labels", Y
    #print "relevant features", rfs

    fig, ax = pl.subplots()
    #pl.xlim(0.0,d-1)
    pl.ylim(-0.4,1.2)
    #print len(linmodel1.reshape(-1))
    ax.plot(range(len(linmodel1.reshape(-1))), linmodel1.reshape(-1), color = 'red')
    ax.plot(range(len(linmodel2.reshape(-1))), linmodel2.reshape(-1), color = 'red')
    ax.plot(range(len(linmodel1.reshape(-1))), linmodel1_cosvr.reshape(-1), color = 'blue')
    ax.plot(range(len(linmodel2.reshape(-1))), linmodel2_cosvr.reshape(-1), color = 'blue')
    if datatype == 'toy':
        ax.plot(range(d), sparsities, color = 'green')
        for k in xrange(norf):
            ax.plot([rfs[k], rfs[k]], [-0.4,1.2], color = 'black')
    elif datatype == 'real':
        ax.plot(range(len(linmodel1.reshape(-1))), sparsitiesList[0], color = 'green')
        ax.plot(range(len(linmodel2.reshape(-1))), sparsitiesList[1], color = 'green')
    

    pl.show()

if __name__ == "__main__":
    example4()
