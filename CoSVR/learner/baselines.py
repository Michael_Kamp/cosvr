'''
Created on 20.04.2015

@author: Michael Kamp
'''

import numpy as np
#from datetime import datetime
from learner import Learner
#from kernel import GaussKernel, LinearKernel
from svr import svr_train, svr_predict
from rlsr import rlsr_train, rlsr_predict
#import collections as cl

class SingleLearner(Learner):
    def __init__(self, **kwargs):        
        Learner.__init__(self, **kwargs)
        self.processParameter(kwargs)
        self.identifier = "Generic Single Learner (please replace)"
        
    def train(self, L, U, data):
        self.views = data.views
        Ks = []
        for v in self.views:
            vidx = data.getViewIndex(v)
            if hasattr(self.k, 'sigma'):
                self.k.setParams(sigma = self.sigma[vidx])
            self.k.precomputeKernel(data, vidx)
            K = self.k.getK(data, vidx, L)
            if self.checkKernel:
                self.validateKernel(K)
            Ks.append(K)            
        for v in self.views:     
            vidx = data.getViewIndex(v)
            kwargs = {'K':Ks[vidx], 'L':L, 'y':data.getYL(L)}
            for param in self.viewIndependentParams:
                if param != 'k':
                    kwargs[param] = self.getParamVal(param, data)
            for param in self.viewDependentParams:
                kwargs[param] = self.getParamVal(param, data)[vidx]          
            SVs, Coeffs = self.train_(**kwargs)
            self.svs[vidx] = SVs
            self.coeffs[vidx] = Coeffs
        return len(self.svs[0]) > 0
            
    def predict(self, Xidx, data):
        if self.views == []:
            self.views = data.views
        y_preds = []
        for vidx in xrange(len(self.views)):            
            if len(self.svs[vidx]) == 0:
                print " Method ",self," has no support vectors. ",
                return np.zeros(len(Xidx))                
            y = self.predict_(Xidx, vidx, data)
            y_preds.append(y)
        return np.array(y_preds)
    
    def getNumberOfSupportVectors(self):
        if hasattr(self, 'svs'):
            num = []
            for view in self.svs.keys():
                num.append(len(self.svs[view]))
            return num
        else:
            return [0]*len(self.views)
              
class SingleRLSR(SingleLearner):
    '''Initializes the Single View RLSR. 
    Parameters:
    paramRange    -    dictionary. For every parameter (key) it holds a list of possible parameters for parameter evaluation. 
    k             -    kernel
    nu            -    regularization parameter of the support vector regression.    
    sigma         -    kernel width for Gaussian kernel
    '''      
    def __init__(self, **kwargs):  
        self.configureParams(viewIndependentParams = ['k'], viewDependentParams = ['nu'], kernelDependentParams = ['sigma'])       
        self.svs = {}
        self.coeffs = {}
        SingleLearner.__init__(self, **kwargs)
        self.processParameter(kwargs)
        self.identifier = "SingleRLSR"
        
        
    def train_(self, **kwargs):
        svs, coeffs = rlsr_train(kwargs['K'], kwargs['L'], kwargs['y'], kwargs['nu'])
        return svs, coeffs                
            
    def predict_(self, X, vidx, data):
        if hasattr(self, 'sigma'):
            self.k.setParams(sigma = self.getParamVal('sigma', data, vidx))
        K = self.k.getK(data, vidx, X, self.svs[vidx])    
        if self.checkKernel:
            self.validateKernel(K)              
        y_pred = rlsr_predict(K, self.coeffs[vidx])
        return np.array(y_pred)            

class MeanRLSR(SingleRLSR):
    '''Initializes the Mean RLSR. 
    Parameters:
    paramRange    -    dictionary. For every parameter (key) it holds a list of possible parameters for parameter evaluation. 
    k             -    kernel
    nu            -    regularization parameter of the support vector regression.    
    sigma         -    kernel width for Gaussian kernel
    '''      
    def __init__(self, **kwargs):
        SingleRLSR.__init__(self, **kwargs)
        self.identifier = "MeanRLSR"
        
    def predict(self, Xidx, data):
        if self.views == []:
            self.views = data.views
        y_preds = []
        for vidx in xrange(len(self.views)):
            y = self.predict_(Xidx, vidx, data)
            y_preds.append(y)
        return np.average(np.array(y_preds), axis = 0)
    
class SingleSVR(SingleLearner):
    '''Initializes the Single View SVR. 
    Parameters:
    paramRange    -    dictionary. For every parameter (key) it holds a list of possible parameters for parameter evaluation. 
    k             -    kernel
    nu            -    regularization parameter of the support vector regression.
    epsL           -    specifies the epsilon-tube within which no penalty is associated in the training loss function with points predicted within a distance epsilon from the actual value.
    sigma         -    kernel width for Gaussian kernel
    '''      
    def __init__(self, **kwargs):     
        self.configureParams(viewIndependentParams = ['k', 'epsL'], viewDependentParams = ['nu'], kernelDependentParams = ['sigma'])    
        self.svs = {}
        self.coeffs = {}   
        SingleLearner.__init__(self, **kwargs)
        self.processParameter(kwargs)
        self.identifier = "SingleSVR"                 
             
    def train_(self, **kwargs):
        svs, coeffs = svr_train(kwargs['K'], kwargs['L'], kwargs['y'], kwargs['nu'], kwargs['epsL'])
        return svs, coeffs                
            
    def predict_(self, X, vidx, data):
        if hasattr(self, 'sigma'):
            self.k.setParams(sigma = self.getParamVal('sigma', data, vidx)) 
        K = self.k.getK(data, vidx, X, self.svs[vidx])
        if self.checkKernel:
            self.validateKernel(K)             
        y_pred = svr_predict(K, self.coeffs[vidx])
        return np.array(y_pred)      
            
class MeanSVR(SingleSVR):
    '''Initializes the Single View SVR. 
    Parameters:
    paramRange    -    dictionary. For every parameter (key) it holds a list of possible parameters for parameter evaluation. 
    k             -    kernel
    nu            -    regularization parameter of the support vector regression.
    epsL           -    specifies the epsilon-tube within which no penalty is associated in the training loss function with points predicted within a distance epsilon from the actual value.
    sigma         -    kernel width for Gaussian kernel
    '''      
    def __init__(self, **kwargs):        
        SingleSVR.__init__(self, **kwargs)
        self.identifier = "MeanSVR"
        
        
    def predict(self, Xidx, data):
        if self.views == []:
            self.views = data.views
        y_preds = []
        for vidx in xrange(len(self.views)):
            y = self.predict_(Xidx, vidx, data)
            y_preds.append(y)
        return np.average(np.array(y_preds), axis = 0)
    
class ConcatRLSR(SingleRLSR):
    def __init__(self, **kwargs):    
        self.configureParams(viewIndependentParams = ['k', 'nu', 'sigma'], viewDependentParams = [], kernelDependentParams = ['sigma'])                
        SingleRLSR.__init__(self, **kwargs)
        self.processParameter(kwargs)
        self.identifier = "ConcatRLSR"
        
    def train(self, L, U, data):
        X = data.sX[L]
        if hasattr(self, 'sigma'):
            self.k.setParams(sigma = self.getParamVal('sigma', data))
        K = self.k.computeKernelMatrix(X, X)
        if self.checkKernel:
            self.validateKernel(K)           
        #y = data.getYL(L)        
        kwargs = {'K':K, 'Xidx':L, 'y':data.getYL(L)}
        for param in self.viewIndependentParams:
            if param != 'k' and param not in self.kernelDependentParams:
                kwargs[param] = self.getParamVal(param, data)
        for param in self.viewDependentParams:
            kwargs[param] = self.getParamVal(param, data)        
        SVs, Coeffs = rlsr_train(**kwargs)
        self.svs = SVs
        self.coeffs = Coeffs
        return len(self.svs) > 0
        
    def predict(self, Xidx, data):
        if self.views == []:
            self.views = data.views
        if len(self.svs) == 0:
            print " Method ",self," has no support vectors. ",
            return np.zeros(len(Xidx))
        X = data.sX[Xidx]
        SV = data.sX[self.svs]
        if hasattr(self, 'sigma'):
            self.k.setParams(sigma = self.getParamVal('sigma', data))
        K = self.k.computeKernelMatrix(X, SV)  
        if self.checkKernel:
            self.validateKernel(K)     
        y_pred = rlsr_predict(K, self.coeffs)
        return np.array(y_pred)    
        
    def getNumberOfSupportVectors(self):
        if hasattr(self, 'svs'):
            return len(self.svs)
        else:
            return 0
    
class ConcatSVR(SingleSVR):
    '''Initializes the Single View SVR. 
    Parameters:
    paramRange    -    dictionary. For every parameter (key) it holds a list of possible parameters for parameter evaluation. 
    k             -    kernel
    nu            -    regularization parameter of the support vector regression.
    epsL           -    specifies the epsilon-tube within which no penalty is associated in the training loss function with points predicted within a distance epsilon from the actual value.
    sigma         -    kernel width for Gaussian kernel
    '''      
    def __init__(self, **kwargs): 
        self.configureParams(viewIndependentParams = ['k','epsL', 'nu', 'sigma'], viewDependentParams = [], kernelDependentParams = ['sigma'])                   
        SingleSVR.__init__(self, **kwargs)
        self.processParameter(kwargs)
        self.identifier = "ConcatSVR"
    
    def train(self, L, U, data):
        X = data.sX[L]
        if hasattr(self, 'sigma'):
            self.k.setParams(sigma = self.getParamVal('sigma', data))
        K = self.k.computeKernelMatrix(X, X)
        if self.checkKernel:
            self.validateKernel(K)           
        kwargs = {'K':K, 'Xidx':L, 'y':data.getYL(L)}
        for param in self.viewIndependentParams:
            if param != 'k' and param not in self.kernelDependentParams:
                kwargs[param] = self.getParamVal(param, data)
        for param in self.viewDependentParams:
            kwargs[param] = self.getParamVal(param, data)        
        SVs, Coeffs = svr_train(**kwargs)
        self.svs = SVs
        self.coeffs = Coeffs
        return len(self.svs) > 0
        
    def predict(self, Xidx, data):
        if len(self.svs) == 0:
            print " Method ",self," has no support vectors. ",
            return np.zeros(len(Xidx))
        X = data.sX[Xidx]
        SV = data.sX[self.svs]
        if hasattr(self, 'sigma'):
            self.k.setParams(sigma = self.getParamVal('sigma', data))
        K = self.k.computeKernelMatrix(X, SV)
        #print "Kconcat", K
        if self.checkKernel:
            self.validateKernel(K)     
        y_pred = svr_predict(K, self.coeffs)
        return np.array(y_pred)    
        
    def getNumberOfSupportVectors(self):
        if hasattr(self, 'svs'):
            return len(self.svs)
        else:
            return 0