'''
Created on 23.04.2015

@author: Katrin Ullrich
'''

import numpy as np
from cvxopt import matrix, solvers

def svr_train(K, Xidx, y, nu, epsL):
    #K...kernel matrix over labelled examples, K = original gram matrix + np.ones((n,n))
    #L...indices of the data rows in the data object
    #y...vector of labels, 1-dimensional np.array of length n
    #nu...norm-regularization parameter
    #epsL...margin parameter of epsilon-insensitive loss
    n = len(y) #number of training examples
    I_n = np.identity(n)
    I_nn = np.identity(2*n)
    H_1 = np.concatenate((I_n, -I_n), axis=1)
    H_2 = np.concatenate((I_n, I_n), axis=1)
    P = matrix(1/float(nu) * np.dot( np.dot(H_1.transpose(), K) , H_1))
    q = matrix((epsL * np.sum(H_2, axis=0) - np.dot(y.reshape(1,-1), H_1)).reshape(-1))
    G = matrix(np.concatenate((I_nn, -I_nn), axis=0))
    h = matrix(np.concatenate((np.ones(2*n), np.zeros(2*n)), axis=0))
    solvers.options['show_progress'] = False
    #solvers.options['abstol'] = 1e-7 #default 1e-7
    duals = solvers.qp(P, q, G, h)['x'] #x = (\alpha_1,...,\alpha_n,\hat{\alpha}_1,...,\hat{\alpha}_n )^T
    #print "duals", duals
    #print "nu", nu
    pi = 1/float(nu) * np.dot(H_1, np.array(duals).reshape(-1)) #calculation of kernel expansion \pi via alpha-variables
    lim = 0.00001 #tolerance parameter, very small entries are assumed to be zero
    svs = []
    coeffs = []
    for i in xrange(n):
        if np.abs(pi[i]) > lim:
            svs.append(Xidx[i])
            coeffs.append(pi[i])
    #svs...list of support vector indices
    #coeffs...list of support vector coefficients
    #print "svr: n",n, "n.o.svs", len(svs), float(nu)*np.array(coeffs) #kernel expansion coefficients, not equal to dual variables
    return np.array(svs), np.array(coeffs)

#def svr_predict(K, coeffs):
#    Y = []
#    for x in xrange(len(K)):
#        y = 0.0
#        for i in xrange(len(K[0])):
#            y += coeffs[i] * K[x][i]
#        Y.append(y)
#    return np.array(Y)

def svr_predict(K, coeffs):
    pred = np.dot(K, coeffs.reshape(-1,1)).reshape(-1)
    return pred.reshape(-1)

