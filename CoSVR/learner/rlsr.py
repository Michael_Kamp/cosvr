'''
Created on 25.04.2015

@author: Katrin Ullrich
'''

import numpy as np
from numpy import linalg

        
def rlsr_train(K, Xidx, y, nu):
    #print "concat view"
    #print K
    n = len(K)
    #print "nu", nu
    A = nu*np.identity(n) + K
    coeffs = linalg.solve(A,y)
    svs = Xidx
    return np.array(svs), np.array(coeffs)

def rlsr_predict(K, coeffs):
    pred = np.dot(K, coeffs.reshape(-1,1))
    return pred.reshape(-1)
    
