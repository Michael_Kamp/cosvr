'''
Created on 29.04.2015

@author: Katrin Ullrich
'''

import numpy as np
#import cvxopt
from cvxopt import matrix, solvers
#from numpy import random
from learner import Learner
#from numpy.oldnumeric.alter_code1 import svspc2
#from kernel import GaussKernel, LinearKernel
#import collections as cl
#import itertools

class CoSVRBase(Learner):
    '''Initializes the CoSVR. 
    Parameters:
    possParams    -    dictionary. For every parameter (key) it holds a list of possible parameters for parameter evaluation. 
    k             -    kernel
    nu            -    regularization parameter of the support vector regression.
    lmbda         -    parameter defining influence of co-regularization.
    epsL          -    specifies the epsilon-tube within which no penalty is associated for labeled points in the training loss function with points predicted within a distance epsilon from the actual value.
    epsU          -    specifies the epsilon-tube within which no penalty is associated for unlabeled points in the training loss function with points predicted within a distance epsilon from the actual value.
    sigma         -    kernel width for Gaussian kernel
    '''      
    def __init__(self, **kwargs):        
        self.svs = {}
        self.coeffs = {}
        Learner.__init__(self, **kwargs)       
        self.processParameter(kwargs)        
        self.identifier = "CoSVRBase Class (please replace)"
    
    def train(self, L, U, data):
        self.views = data.views
        for param in self.viewDependentParams:
            if len(getattr(self,param)) == 1 and len(self.views) > 1:
                setattr(self,param,np.array(getattr(self,param)*len(self.views)))
        for param in self.kernelDependentParams:
            if self.kernelRequiresParam(param):
                if len(getattr(self,param)) == 1 and len(self.views) > 1:
                    setattr(self,param,np.array(getattr(self,param)*len(self.views)))        
        Ks = []
        Xidx = np.hstack((L,U))
        for v in self.views:
            vidx = data.getViewIndex(v)
            if hasattr(self, 'sigma'):
                self.k.setParams(sigma = self.getParamVal('sigma', data)[vidx])
            self.k.precomputeKernel(data, vidx)            
            K = self.k.getK(data, vidx, Xidx)
            if self.checkKernel:
                self.validateKernel(K)
            Ks.append(K)      
        kwargs = {'Ks':Ks, 'y':data.getYL(L), 'Xidx':Xidx, 'U':U}
        for param in self.viewIndependentParams:
            if param != 'k':
                kwargs[param] = self.getParamVal(param, data)
        for param in self.viewDependentParams:
            kwargs[param] = self.getParamVal(param, data)                          
        SVs, Coeffs = self.train_(**kwargs)
        self.svs = SVs
        self.coeffs = Coeffs
        return len(self.svs) > 0  
            
    def train_(self, **kwargs):                                                               
        return [np.array([])],[np.array([])]
            
    def predict(self, Xidx, data):
        Ks = []
        for v in self.views:
            vidx = data.getViewIndex(v)
            if len(self.svs[vidx]) == 0:
                print "Warning: #SVs is 0 for view",vidx,
                return np.zeros(len(Xidx))     
            if hasattr(self, 'sigma'):
                self.k.setParams(sigma = self.getParamVal('sigma', data, vidx))      
            K = self.k.getK(data, vidx, Xidx, self.svs[vidx])
            if self.checkKernel:
                self.validateKernel(K)     
            Ks.append(K)             
        y_pred = self.predict_(Ks)
        return np.array(y_pred)     
    
class EpsCoSVR(CoSVRBase):
    def __init__(self, **kwargs):  
        self.configureParams(viewIndependentParams = ['k','lmbda','epsL','epsU'], viewDependentParams = ['nu'], kernelDependentParams = ['sigma'])
        CoSVRBase.__init__(self, **kwargs)
        self.processParameter(kwargs)
        self.identifier = "EpsCoSVR"
    
    def train_(self, **kwargs):
        return eps_cosvr_train(kwargs['Ks'], kwargs['Xidx'], kwargs['y'], kwargs['epsL'], kwargs['epsU'], kwargs['nu'], kwargs['lmbda'])    
    
    def predict_(self, Ks):
        return cosvr_predict(Ks, self.coeffs)

class EpsRelCoSVR(CoSVRBase):
    def __init__(self, **kwargs):        
        self.configureParams(viewIndependentParams = ['k','lmbda','epsL','epsU'], viewDependentParams = ['nu'], kernelDependentParams = ['sigma'])
        CoSVRBase.__init__(self, **kwargs)
        self.processParameter(kwargs)
        self.identifier = "EpsRelCoSVR"
    
    """
    def train_(self, **kwargs):
        try:
            svs, coeffs = eps_cosvr_rel_train(kwargs['Ks'], kwargs['Xidx'], kwargs['y'], kwargs['epsL'], kwargs['epsU'], kwargs['nu'], kwargs['lmbda'])
        except:
            print kwargs['epsL'], kwargs['epsU'], kwargs['nu'], kwargs['lmbda']
            print [],[]
        return svs, coeffs
    """
    
    def train_(self, **kwargs):
        return eps_cosvr_rel_train(kwargs['Ks'], kwargs['Xidx'], kwargs['y'], kwargs['epsL'], kwargs['epsU'], kwargs['nu'], kwargs['lmbda'])  
    
    def predict_(self, Ks):
        return cosvr_predict(Ks, self.coeffs)
    
class L2CoSVR(CoSVRBase):
    def __init__(self, **kwargs):       
        self.configureParams(viewIndependentParams = ['k','lmbda','epsL'], viewDependentParams = ['nu'], kernelDependentParams = ['sigma']) 
        CoSVRBase.__init__(self, **kwargs)
        self.processParameter(kwargs)        
        self.identifier = "L2CoSVR"
    
    def train_(self, **kwargs):
        return l2_cosvr_train(kwargs['Ks'], kwargs['Xidx'], kwargs['y'], kwargs['epsL'], kwargs['nu'], kwargs['lmbda'])    
    
    def predict_(self, Ks):
        return cosvr_predict(Ks, self.coeffs)
    
    
class L2RelCoSVR(CoSVRBase):
    def __init__(self, **kwargs):       
        self.configureParams(viewIndependentParams = ['k','lmbda','epsL'], viewDependentParams = ['nu'], kernelDependentParams = ['sigma'])  
        CoSVRBase.__init__(self, **kwargs)
        self.processParameter(kwargs)
        self.identifier = "L2RelCoSVR"
    
    def train_(self, **kwargs):
        return l2_cosvr_rel_train(kwargs['Ks'], kwargs['Xidx'], kwargs['y'], kwargs['epsL'], kwargs['nu'], kwargs['lmbda'])    
    
    def predict_(self, Ks):
        return cosvr_predict(Ks, self.coeffs)
    

class SigmaCoSVR(CoSVRBase):
    def __init__(self, **kwargs):      
        self.configureParams(viewIndependentParams = ['k','lmbda','epsL'], viewDependentParams = ['nu'], kernelDependentParams = ['sigma'])  
        self.unlabeledTrainingIdx = []
        CoSVRBase.__init__(self, **kwargs)
        self.processParameter(kwargs)        
        self.identifier = "SigmaCoSVR"
    
    def train_(self, **kwargs):
        Ks = kwargs['Ks']
        if len(Ks) > 2:
            print "Error: SigmaCoSVR can only work with 2 views but ",len(Ks)," views have been passed. I'm taking only the first two views."
            Ks = Ks[:2]
        U =  kwargs['U']
        Ksigma = calc_sigma_kernel(Ks, kwargs['nu'], kwargs['lmbda'], U)
        self.unlabeledTrainingIdx = U
        return sigma_cosvr_train(Ksigma, kwargs['Xidx'], kwargs['y'], kwargs['epsL'])    
     
    
    def predict(self, Xidx, data):
        if not hasattr(self.svs, '__len__') or len(self.svs) == 0 or len(self.svs[0]) == 0:
            print "Warning: #SVs is 0",
            return np.zeros(len(Xidx))
        K_XSVs  = []
        K_Us    = []
        K_UXs   = []
        K_USVs  = []
        U = self.unlabeledTrainingIdx
        for v in self.views:            
            vidx = data.getViewIndex(v)
            if hasattr(self, 'sigma'):
                self.k.setParams(sigma = self.getParamVal('sigma', data, vidx))
            K_XSV = self.k.getK(data, vidx, Xidx, self.svs[0])
            K_U = self.k.getK(data, vidx, U, U)
            K_UX = self.k.getK(data, vidx, U, Xidx)
            K_USV = self.k.getK(data, vidx, U, self.svs[0])
            K_XSVs.append(K_XSV)
            K_Us.append(K_U)
            K_UXs.append(K_UX)            
            K_USVs.append(K_USV)
        if len(K_XSVs) > 2:
            print "Error: SigmaCoSVR can only work with 2 views but ",len(K_XSVs)," views have been passed. I'm taking only the first two views."
            K_XSVs = K_XSVs[:2]
            K_Us   = K_Us[:2]
            K_UXs  = K_UXs[:2]  
            K_USVs  = K_USVs[:2]
        Ksigma = fuse_sigma_kernel_for_prediction(K_XSVs, K_Us, K_UXs, K_USVs, self.nu, self.lmbda)
        if self.checkKernel:
            self.validateKernel(Ksigma)     
        y_pred = sigma_cosvr_predict(Ksigma, self.coeffs)
        return np.array(y_pred)

def cosvr_predict(Ks, coeffs):
    #Ks ... list of matrices
    #coeffs ... list of coefficient lists
    M = len(Ks)
    m = len(Ks[0])
    pred = np.zeros(m)
    for v in xrange(M):
        pred += 1./float(M)*np.dot(Ks[v], coeffs[v].reshape(-1,1).reshape(-1))
    return pred

def sigma_cosvr_predict(K, coeffs):
    #K ... fused sigma kernel matrix
    #K ... list of coefficients
    pred = np.dot(K, coeffs.reshape(-1,1)).reshape(-1)
    return pred

def eps_cosvr_train(Ks, Xidx, y, epsL, epsU, nus, lam):
    #Ks: list of view kernel matrices, 2-dimensional np.array Ks[v] \in R^{(n+m)x(n+m)}
    #y: 1-dimensional np.array of labels
    #epsL, epsU: epsilon parameter for labelled and unlabelled loss function
    #nus: list of function norm regularization parameter in views
    #lam: lambda parameter for unlabelled error
    n = len(y) #number of labelled instances
    nPm = len(Ks[0]) #n+m
    m = nPm - n #number of unlabelled instances
    #M = len(nus) #number of views
    M = len(Ks)
    TwoMn = 2*M*n
    MTwom = M*M*m
    dim = TwoMn + MTwom
    Mm = M*m
    K = np.zeros((M*(nPm), M*(nPm)))
    H1 = np.zeros((M*nPm, dim))
    H2 = np.zeros((M*nPm, dim))
    H3 = np.zeros((nPm, M*nPm))    
    One_eps = np.concatenate((epsL*np.ones(n), epsU*np.ones(m)), axis=0)
    A = np.zeros((Mm, dim))
    b = np.zeros(Mm)
    G = np.concatenate((np.identity(dim), -np.identity(dim)), axis=0)
    h = np.concatenate((np.ones(TwoMn), lam*np.ones(MTwom), np.zeros(dim)), axis=0)
    nu_vector = np.ones(M*nPm)
    
    for v in xrange(M):
        K[v*nPm:(v+1)*nPm, v*nPm:(v+1)*nPm] = 1/float(nus[v]) * Ks[v]
        #K[v*nPm:(v+1)*nPm, v*nPm:(v+1)*nPm] = 1/float(nus) * Ks[v]
        H1[v*nPm:v*nPm + n, v*n:(v+1)*n] = np.identity(n)
        H1[v*nPm:v*nPm + n, M*n + v*n:M*n + (v+1)*n] = - np.identity(n)
        H2[v*nPm:v*nPm + n, v*n:(v+1)*n] = np.identity(n)
        H2[v*nPm:v*nPm + n, M*n + v*n: M*n + v*n + n] = np.identity(n)
        H3[:,v*nPm:(v+1)*nPm] = np.identity(nPm)
        A[v*m:(v+1)*m, TwoMn + v*Mm + v*m: TwoMn + v*Mm + (v+1)*m] = np.identity(m)
        nu_vector[v*(nPm): (v+1)*nPm] = 1/float(nus[v])*np.ones(nPm)
        #nu_vector[v*(nPm): (v+1)*nPm] = 1/float(nus)*np.ones(nPm)
        for u in xrange(M):
            if u != v:
                H1[v*nPm+n:v*nPm+nPm, TwoMn + v*Mm + u*m:TwoMn + v*Mm + (u+1)*m] = np.identity(m)
                H1[v*nPm+n:v*nPm+nPm, TwoMn + u*Mm + v*m:TwoMn + u*Mm + (v+1)*m] = -np.identity(m)
                H2[v*nPm+n:v*nPm+nPm, TwoMn + v*Mm + u*m:TwoMn + v*Mm + (u+1)*m] = np.identity(m)

    P = np.dot(np.dot(H1.transpose(), K), H1)
    y0 = np.concatenate((y, np.zeros(m)), axis=0)
    q = np.dot(np.dot(One_eps.reshape(-1), H3), H2) - np.dot(np.dot(y0.reshape(-1), H3), H1)
    P = matrix(P)
    q = matrix(q)
    A = matrix(A)
    b = matrix(b)
    G = matrix(G)
    h = matrix(h)
    
    solvers.options['show_progress'] = False
    #solvers.options['abstol'] = 1e-7 #default 1e-7
    duals = solvers.qp(P, q, G, h, A, b)['x'] 
    pi = nu_vector * np.dot(H1, np.array(duals).reshape(-1,1)).reshape(-1) #calculation of kernel expansion \pi via alpha- and gamma-variables
    lim = 0.0000000001
    svs = []
    coeffs = []
    #for i in xrange(M*nPm):
    #    if np.abs(pi[i]) > lim:
    #        svs.append(i)
    #        coeffs.append(pi[i])
    for v in xrange(M):
        view_svs = []
        view_coeffs = []
        for i in xrange(nPm):
            index = v*nPm + i
            if np.abs(pi[index]) > lim:
                view_svs.append(Xidx[i])
                view_coeffs.append(pi[index])
        svs.append(np.array(view_svs))
        coeffs.append(np.array(view_coeffs))
    #svs...list of support vector indices
    #coeffs...list of support vector coefficients
    return svs, coeffs


def l2_cosvr_train(Ks, Xidx, y, epsL, nus, lam):
    n = len(y) #number of labelled instances
    nPm = len(Ks[0]) #n+m
    m = nPm - n #number of unlabelled instances
    M = len(nus) #number of views
    TwoMn = 2*M*n
    MTwom = M*M*m
    dim = TwoMn + MTwom
    Mm = M*m
    Mn = M*n
    
    #"""start new implementation
    K = np.zeros((M*(nPm), M*(nPm)))
    Hminus = np.zeros((Mn, dim))
    Hplus = np.zeros((Mn, dim))
    H = np.zeros((M*nPm, dim))
    Hi = np.zeros((dim, dim))
    Hi[2*Mn:,2*Mn:] = np.identity(MTwom)
    Hgamma = np.zeros((Mm, dim))
    Hgamma1 = np.zeros((Mm, dim))
    Hgamma2 = np.zeros((Mm, dim))
    Halpha = np.zeros((n, M*n))
    G = np.zeros((4*Mn,dim))
    A = np.zeros((MTwom, dim))
    Hnu = np.zeros((M*nPm, M*nPm))
    Hvs = [] #list of Hv matrices
    for v in xrange(M):
        K[v*nPm:(v+1)*nPm, v*nPm:(v+1)*nPm] = 1/float(nus[v])*Ks[v]
        Hplus[v*n:(v+1)*n, v*2*n:v*2*n+n] = np.identity(n)
        Hplus[v*n:(v+1)*n, v*2*n+n:(v+1)*2*n] = np.identity(n)
        Hminus[v*n:(v+1)*n, v*2*n:v*2*n+n] = np.identity(n)
        Hminus[v*n:(v+1)*n, v*2*n+n:(v+1)*2*n] = -np.identity(n)
        Hgamma1[:, TwoMn + v*Mm:TwoMn + (v+1)*Mm] = np.identity(Mm)
        for u in xrange(M):
            Hgamma2[v*m:(v+1)*m, TwoMn + v*Mm + u*m: TwoMn + v*Mm + (u+1)*m] = np.identity(m)
        Halpha[:,v*n:(v+1)*n] = np.identity(n)
        Hnu[v*nPm:(v+1)*nPm, v*nPm:(v+1)*nPm] = 1/float(nus[v])*np.identity(nPm)
    Hgamma = Hgamma1 - Hgamma2   
    for v in xrange(M):
        Hv = np.zeros((nPm, dim))
        Hv[:n,:] = Hminus[v*n:(v+1)*n,:]
        Hv[n:,:] = Hgamma[v*m:(v+1)*m,:]
        Hvs.append(Hv)
        H[v*nPm:(v+1)*nPm, :] = Hv
    for v in xrange(M):
        for u in xrange(M):
            Huv = np.zeros((m, dim))
            Huv[:, TwoMn + u*Mm + v*m: TwoMn + u*Mm + (v+1)*m] = np.identity(m)
            A[u*Mm + v*m: u*Mm + (v+1)*m, :] = 2*lam/float(nus[u])*np.dot(Ks[u][n:,:], Hvs[u]) - 2*lam/float(nus[v]) * np.dot(Ks[v][n:,:], Hvs[v]) - Huv
    P = np.dot(H.transpose(), np.dot(K, H))   
    #print np.shape(np.sum(np.dot(Halpha, Hplus), axis=0))
    #print np.shape(np.dot(y.reshape((1,-1)), np.dot(Halpha, Hminus)))
    q = (epsL * np.sum(np.dot(Halpha, Hplus), axis=0) - np.dot(y.reshape((1,-1)), np.dot(Halpha, Hminus))).reshape((-1,1))#column vector
    h = np.append(np.ones(TwoMn), np.zeros(TwoMn), axis=0) #line vector
    G[:TwoMn,:TwoMn] = np.identity(TwoMn)
    G[TwoMn:,:TwoMn] = -np.identity(TwoMn)
    b = np.zeros(MTwom) #line vector
       
    #"""
    """start old implementation
    n = len(y) #number of labelled instances
    nPm = len(Ks[0]) #n+m
    m = nPm - n #number of unlabelled instances
    M = len(nus) #number of views
    TwoMn = 2*M*n
    MTwom = M*M*m
    dim = TwoMn + MTwom
    Mm = M*m
    Mn = M*n
    K = np.zeros((M*(nPm), M*(nPm)))
    H1 = np.zeros((M*nPm, dim))
    H2 = np.zeros((M*nPm, dim))
    H3 = np.zeros((nPm, M*nPm))   
    H4 = np.zeros((dim, dim))
    H4[TwoMn:,TwoMn:] = 1/float(2*lam)*np.identity(MTwom)
    G = np.zeros((2*TwoMn, dim))
    G[:TwoMn,:TwoMn] = np.identity(TwoMn)
    G[TwoMn:,:TwoMn] = -np.identity(TwoMn)
    h = np.concatenate((np.ones(TwoMn), np.zeros(TwoMn)), axis=0)
    A = np.zeros((Mm+MTwom, dim))
    b = np.zeros(MTwom) #Mm + MTwom - Mm
    One_eps = np.concatenate((epsL*np.ones(n), np.zeros(m)), axis=0)
    y0 = np.concatenate((y.reshape(-1), np.zeros(m)), axis=0)
    nu_vector = np.ones(M*nPm)

    for v in xrange(M):
        K[v*nPm:(v+1)*nPm, v*nPm:(v+1)*nPm] = 1/float(nus[v])*Ks[v]
        H1[v*nPm: v*nPm+n, v*n:(v+1)*n] = np.identity(n)
        H1[v*nPm: v*nPm+n, Mn + v*n: Mn + (v+1)*n] = -np.identity(n)

        H2[v*nPm: v*nPm+n, v*n:(v+1)*n] = np.identity(n)
        H2[v*nPm: v*nPm+n, Mn + v*n: Mn + (v+1)*n] = np.identity(n)
        H3[:,v*nPm:(v+1)*nPm] = np.identity(nPm)
        A[v*m:(v+1)*m, TwoMn + v*Mm + v*m: TwoMn + v*Mm + (v+1)*m] = np.identity(m)
        nu_vector[v*(nPm): (v+1)*nPm] = 1/float(nus[v])*np.ones(nPm)
        for u in xrange(M):
            if u != v:
                H1[v*nPm + n: (v+1)*nPm, TwoMn + v*Mm + u*m: TwoMn + v*Mm + (u+1)*m] = 2*np.identity(m)
                Huv = np.zeros((m, dim))
                Huv[:, TwoMn + v*Mm + u*m: TwoMn + v*Mm + (u+1)*m] = np.identity(m)
                A[Mm + v*Mm + u*m: Mm + v*Mm + (u+1)*m, :] = (nus[v]/float(4*lam))*Huv + np.dot(Ks[v][n:], H1[v*nPm: (v+1)*nPm,:]) 

    indA = range(0,Mm)
    for v in xrange(M):
        for j in xrange(Mm):
            if (j < v*m) or (j >= (v+1)*m):
                indA.append(Mm + v*Mm + j)
    A = A[indA,:] #zero-lines for gamma_vv deleted

    P = np.dot(np.dot(H1.transpose(), K), H1) + H4
    q = np.dot(np.dot(One_eps.reshape(-1), H3), H2) - np.dot(np.dot(y0.reshape(-1), H3), H1)
    
    
    P = matrix(P)
    q = matrix(q)
    A = matrix(A)
    b = matrix(b)
    G = matrix(G)
    h = matrix(h)
    
    solvers.options['show_progress'] = False
    duals = solvers.qp(P, q, G, h, A, b)['x'] 
    #print "duals l2-cosvr", duals
    pi = nu_vector * np.dot(H1, np.array(duals).reshape(-1,1)).reshape(-1) 
    #print "pi l2-cosvr", pi
    """
    P = matrix(P)
    q = matrix(q)
    A = matrix(A)
    b = matrix(b)
    G = matrix(G)
    h = matrix(h)
    
    solvers.options['show_progress'] = False
    try:
        duals = solvers.qp(P, q, G, h, A, b)['x']
    except:
        svs = [np.array([])]*M
        coeffs = [np.array([])]*M
        print "Warning: qp-solver failed.", epsL, nus, lam,
        return svs, coeffs
    
    pi = np.dot(Hnu, np.dot(H, np.array(duals).reshape(-1,1))).reshape(-1) 
    
    lim = 0.0000000001
    svs = []
    coeffs = []
    for v in xrange(M):
        view_svs = []
        view_coeffs = []
        for i in xrange(nPm):
            index = v*nPm + i
            if np.abs(pi[index]) > lim:
                view_svs.append(Xidx[i])
                view_coeffs.append(pi[index])
        svs.append(np.array(view_svs))
        coeffs.append(np.array(view_coeffs))
    #print "svs", svs
    #print "coeffs", coeffs

    return svs, coeffs


def eps_cosvr_rel_train(Ks, Xidx, y, epsL, epsU, nus, lam):
    n = len(y) #number of labelled instances
    nPm = len(Ks[0]) #n+m
    m = nPm - n #number of unlabelled instances
    #M = len(nus) #number of views
    M = len(Ks)
    TwoMn = 2*M*n
    TwoMm = 2*M*m
    dim = TwoMn + TwoMm
    Mm = M*m
    Mn = M*n
    K = np.zeros((M*(nPm), M*(nPm)))
    H1 = np.zeros((M*nPm, dim))
    H2 = np.zeros((M*nPm, dim))
    H3 = np.zeros((nPm, M*nPm))  
    G = np.concatenate((np.identity(dim), -np.identity(dim)), axis=0)
    h = np.concatenate((np.ones(TwoMn), lam*np.ones(TwoMm), np.zeros(dim)), axis=0)
    One_eps = np.concatenate((epsL*np.ones(n), epsU*np.ones(m)), axis=0)
    y0 = np.concatenate((y.reshape(-1), np.zeros(m)), axis=0)
    nu_vector = np.ones(M*nPm)
    for v in xrange(M):
        K[v*nPm:(v+1)*nPm, v*nPm:(v+1)*nPm] = 1/float(nus[v])*Ks[v]
        #K[v*nPm:(v+1)*nPm, v*nPm:(v+1)*nPm] = 1/float(nus)*Ks[v]
        H1[v*nPm: v*nPm + n, v*n: (v+1)*n] = np.identity(n)
        H1[v*nPm: v*nPm + n, Mn + v*n: Mn + (v+1)*n] = -np.identity(n)
        H2[v*nPm: v*nPm + n, v*n: (v+1)*n] = np.identity(n)
        H2[v*nPm: v*nPm + n, Mn + v*n: Mn + (v+1)*n] = np.identity(n)
        H1[v*nPm + n: (v+1)*nPm, TwoMn + v*m: TwoMn + (v+1)*m] = np.identity(m)
        H1[v*nPm + n: (v+1)*nPm, TwoMn + Mm + v*m: TwoMn + Mm + (v+1)*m] = -np.identity(m)
        H2[v*nPm + n: (v+1)*nPm, TwoMn + v*m: TwoMn + (v+1)*m] = np.identity(m)
        H2[v*nPm + n: (v+1)*nPm, TwoMn + Mm + v*m: TwoMn + Mm + (v+1)*m] = np.identity(m)
        H3[:,v*nPm:(v+1)*nPm] = np.identity(nPm)
        nu_vector[v*(nPm): (v+1)*nPm] = 1/float(nus[v])*np.ones(nPm)
        #nu_vector[v*(nPm): (v+1)*nPm] = 1/float(nus)*np.ones(nPm)
        for u in xrange(M):
            if u != v:
                H1[v*nPm + n: (v+1)*nPm, TwoMn + u*m: TwoMn + (u+1)*m] = -(1/float(M-1))*np.identity(m)
                H1[v*nPm + n: (v+1)*nPm, TwoMn + Mm + u*m: TwoMn + Mm + (u+1)*m] = (1/float(M-1))*np.identity(m)

    P = np.dot(np.dot(H1.transpose(), K), H1)
    q = np.dot(np.dot(One_eps.reshape(-1), H3), H2) - np.dot(np.dot(y0.reshape(-1), H3), H1)

    P = matrix(P)
    q = matrix(q)
    G = matrix(G)
    h = matrix(h)
    
    solvers.options['show_progress'] = False
    try:        
        duals = solvers.qp(P, q, G, h)['x'] 
    except:
        print "QP-solver failed."
        return [],[]
    pi = nu_vector * np.dot(H1, np.array(duals).reshape(-1,1)).reshape(-1) 
    lim = 0.0000000001
    svs = []
    coeffs = []
    for v in xrange(M):
        view_svs = []
        view_coeffs = []
        for i in xrange(nPm):
            index = v*nPm + i
            if np.abs(pi[index]) > lim:
                view_svs.append(Xidx[i])
                view_coeffs.append(pi[index])
        svs.append(np.array(view_svs))
        coeffs.append(np.array(view_coeffs))

    return svs, coeffs

def l2_cosvr_rel_train(Ks, Xidx, y, epsL, nus, lam):
    n = len(y) #number of labelled instances
    nPm = len(Ks[0]) #n+m
    m = nPm - n #number of unlabelled instances
    M = len(nus) #number of views
    TwoMn = 2*M*n
    Mm = M*m
    Mn = M*n
    dim = TwoMn + Mm
    
    #"""start new implementation
    K = np.zeros((M*nPm, M*nPm))
    Hi = np.zeros((dim, dim))
    Hi[TwoMn:, TwoMn:] = np.identity(Mm)
    H = np.zeros((M*nPm, dim))
    Hplus = np.zeros((Mn, dim))
    Hminus = np.zeros((Mn, dim))
    #Hgamma = np.zeros((Mm, dim))
    #Hgamma1 = np.zeros((Mm, dim))
    #Hgamma1[:, TwoMn:] = M/float(M-1)*np.identity(Mm)
    #Hgamma2 = np.zeros((Mm, dim))
    Hvs = []
    G = np.zeros((4*Mn, dim))
    A = np.zeros((Mm, dim))
    Halpha = np.zeros((n, Mn))
    Hgammavs = []
    Hnu = np.zeros((M*nPm, M*nPm))
    for v in xrange(M):
        K[v*nPm:(v+1)*nPm, v*nPm:(v+1)*nPm] = 1/float(nus[v])*Ks[v]
        Hplus[v*n:(v+1)*n, 2*v*n:2*v*n + n] = np.identity(n)
        Hplus[v*n:(v+1)*n, 2*v*n + n:2*(v+1)*n] = np.identity(n)
        Hminus[v*n:(v+1)*n, 2*v*n:2*v*n + n] = np.identity(n)
        Hminus[v*n:(v+1)*n, 2*v*n + n:2*(v+1)*n] = -np.identity(n)
        Hv = np.zeros((nPm, dim))
        Hv[:n, :] = Hminus[v*n:(v+1)*n, :]
        Hv[n:, TwoMn + v*m: TwoMn +(v+1)*m] = M/float(M-1)*np.identity(m)
        for u in xrange(M):
            Hv[n:, TwoMn + u*m: TwoMn + (u+1)*m] = Hv[n:, TwoMn + u*m: TwoMn + (u+1)*m] - 1/float(M-1)*np.identity(m)
        Hvs.append(Hv)
        #print "Hminus", Hminus
        #print v, Hv[0]
        H[v*nPm: (v+1)*nPm, :] = Hv
        Hgammav = np.zeros((m, dim))
        Hgammav[:, TwoMn + v*m: TwoMn + (v+1)*m] = np.identity(m)
        Hgammavs.append(Hgammav)
        Halpha[:, v*n: (v+1)*n] = np.identity(n)
        Hnu[v*nPm:(v+1)*nPm, v*nPm:(v+1)*nPm] = 1/float(nus[v])*np.identity(nPm)
    for v in xrange(M):
        A[v*m: (v+1)*m, :] = - (M*2*lam)/float((M-1)*nus[v])*np.dot(Ks[v][n:,:], Hvs[v]) - Hgammavs[v]
        for u in xrange(M):
            A[v*m: (v+1)*m, :] = A[v*m: (v+1)*m, :] + 2*lam/float((M-1)*nus[u])*np.dot(Ks[u][n:,:], Hvs[u])
    #Hgamma = Hgamma1 - Hgamma2
    P = np.dot(H.transpose(), np.dot(K, H)) + 1/float(2*lam)*Hi
    #q = (epsL * np.sum(np.dot(Halpha, Hplus), axis=0) - np.dot(y.reshape((1,-1)), np.dot(Halpha, Hminus))).reshape((-1,1))#column vector
    q = (epsL*np.dot(Hplus.transpose(), np.dot(Halpha.transpose(), np.ones((n,1)))) - np.dot(Hminus.transpose(), np.dot(Halpha.transpose(), y.reshape((-1,1))))).reshape((-1,1))
    h = np.append(np.ones(TwoMn), np.zeros(TwoMn), axis=0) #line vector
    G[:TwoMn,:TwoMn] = np.identity(TwoMn)
    G[TwoMn:,:TwoMn] = -np.identity(TwoMn)
    b = np.zeros(Mm) #line vector
    
    P = matrix(P)
    q = matrix(q)
    A = matrix(A)
    b = matrix(b)
    G = matrix(G)
    h = matrix(h)
    
    solvers.options['show_progress'] = False
    try:
        duals = solvers.qp(P, q, G, h, A, b)['x']
    except:
        svs = [np.array([])]*M
        coeffs = [np.array([])]*M
        print "Warning: qp-solver failed.", epsL, nus, lam,
        return svs, coeffs
            
    pi = np.dot(Hnu, np.dot(H, np.array(duals).reshape(-1,1))).reshape(-1) 
    #"""
    
    """start old implementation
    K = np.zeros((M*(nPm), M*(nPm)))
    H1 = np.zeros((M*nPm, dim))
    H2 = np.zeros((M*nPm, dim))
    H3 = np.zeros((nPm, M*nPm))  
    H4 = np.zeros((dim, dim))
    H4[TwoMn:,TwoMn:] = 1/float(2*lam)*np.identity(Mm)
    One_eps = np.concatenate((epsL*np.ones(n), np.zeros(m)), axis=0)
    y0 = np.concatenate((y.reshape(-1), np.zeros(m)), axis=0)
    nu_vector = np.zeros(M*nPm)
    G = np.zeros((2*TwoMn, dim))
    G[:TwoMn,:TwoMn] = np.identity(TwoMn)
    G[TwoMn:,:TwoMn] = -np.identity(TwoMn)
    h = np.concatenate((np.ones(TwoMn), np.zeros(TwoMn)), axis=0)
    A = np.zeros((Mm, dim))
    b = np.zeros(Mm)
    for v in xrange(M):
        #print nus[v]
        K[v*nPm:(v+1)*nPm, v*nPm:(v+1)*nPm] = 1/float(nus[v])*Ks[v]
        H1[v*nPm: v*nPm + n, v*n: (v+1)*n] = np.identity(n)
        H1[v*nPm: v*nPm + n, Mn + v*n: Mn + (v+1)*n] = - np.identity(n)
        H2[v*nPm: v*nPm + n, v*n: (v+1)*n] = np.identity(n)
        H2[v*nPm: v*nPm + n, Mn + v*n: Mn + (v+1)*n] = np.identity(n)
        H1[v*nPm + n: (v+1)*nPm, TwoMn + v*m: TwoMn + (v+1)*m] = np.identity(m)
        Hv = np.zeros((m, dim))
        Hv[:, TwoMn + v*m: TwoMn + (v+1)*m] = np.identity(m)
        A[v*m: (v+1)*m, :] = (nus[v]/float(2*lam))*Hv + np.dot(Ks[v][n:], H1[v*nPm: (v+1)*nPm,:]) 
        nu_vector[v*(nPm): (v+1)*nPm] = 1/float(nus[v])*np.ones(nPm)
        for u in xrange(M):
            if u != v:
                H1[v*nPm + n: (v+1)*nPm, TwoMn + u*m: TwoMn + (u+1)*m] = -(1/float(M-1))*np.identity(m)

    P = np.dot(np.dot(H1.transpose(), K), H1) + H4
    q = np.dot(np.dot(One_eps.reshape(-1), H3), H2) - np.dot(np.dot(y0.reshape(-1), H3), H1)

    P = matrix(P)
    q = matrix(q)
    A = matrix(A)
    b = matrix(b)
    G = matrix(G)
    h = matrix(h)
    
    solvers.options['show_progress'] = False
    try:
        duals = solvers.qp(P, q, G, h, A, b)['x']
    except:
        svs = [np.array([])]*M
        coeffs = [np.array([])]*M
        print "Warning: qp-solver failed.", epsL, nus, lam,
        return svs, coeffs
            
    pi = nu_vector * np.dot(H1, np.array(duals).reshape(-1,1)).reshape(-1) 
    #print "pi l2-cosvr-rel", pi
    """
    
    lim = 0.0000000001 #tolerance parameter, very small entries are assumed to be zero
    svs = []
    coeffs = []
    for v in xrange(M):
        view_svs = []
        view_coeffs = []
        for i in xrange(nPm):
            index = v*nPm + i
            if np.abs(pi[index]) > lim:
                view_svs.append(Xidx[i])
                view_coeffs.append(pi[index])
        svs.append(np.array(view_svs))
        coeffs.append(np.array(view_coeffs))
    #print svs
    #print coeffs
    return svs, coeffs
    

def calc_sigma_kernel(Ks, nus, lam, U):
    #only 2-view method!!!
    #Ks ... list of view kernels K_1 and K_2 of size (n+m)x(n+m)
    #n ... number of training examples
    m = len(U)
    n = len(Ks[0]) - m
    if not hasattr(nus, '__len__'):
        nus = [nus]*2
    if len(Ks) > 2:
        print "Error: too many views"
        return None
    else:
        #D = 1/float(nus[0])*Ks[0][n:] - 1/float(nus[1])*Ks[1][n:]
        D = 1/float(nus[0])*Ks[0][n:] - 1/float(nus[1])*Ks[1][n:]
        #print "nus", nus
        #print "lam", lam
        #H = np.linalg.inv(1/float(lam)*np.identity(m) + 1/float(nus[0])*Ks[0][n:,n:] + 1/float(nus[1])*Ks[1][n:,n:]) 
        #K = 1/float(nus[0])*Ks[0] + 1/float(nus[1])*Ks[1] - np.dot(np.dot(D.transpose(), H), D)
        H = np.linalg.inv(1/float(lam)*np.identity(m) + 1/float(nus[0])*Ks[0][n:,n:] + 1/float(nus[1])*Ks[1][n:,n:]) 
        K = 1/float(nus[0])*Ks[0] + 1/float(nus[1])*Ks[1] - np.dot(np.dot(D.transpose(), H), D)
        return K

def fuse_sigma_kernel_for_prediction(K_XSVs, K_Us, K_UXs, K_USVs, nus, lam):
    m = len(K_Us[0])
    #Dl = 1/float(nus[0])*K_UXs[0] - 1/float(nus[1])*K_UXs[1]
    #Dr = 1/float(nus[0])*K_USVs[0] - 1/float(nus[1])*K_USVs[1]
    #H = np.linalg.inv(1/float(lam)*np.identity(m) + 1/float(nus[0])*K_Us[0] + 1/float(nus[1])*K_Us[1])
    #K = 1/float(nus[0])*K_XSVs[0] + 1/float(nus[1])*K_XSVs[1] - np.dot(np.dot(Dl.transpose(), H), Dr)
    Dl = 1/float(nus[0])*K_UXs[0] - 1/float(nus[1])*K_UXs[1] #x-indices: all u's, y-indices: all x's
    Dr = 1/float(nus[0])*K_USVs[0] - 1/float(nus[1])*K_USVs[1] #x-indices: all u's, y-indices: als sv's
    H = np.linalg.inv(1/float(lam)*np.identity(m) + 1/float(nus[0])*K_Us[0] + 1/float(nus[1])*K_Us[1]) #H independent of x's and sv's
    K = 1/float(nus[0])*K_XSVs[0] + 1/float(nus[1])*K_XSVs[1] - np.dot(np.dot(Dl.transpose(), H), Dr) #x-indices: all x's, y-indices: all sv's
    return K

def sigma_cosvr_train(K, Xidx, y, epsL):
    #K ... fused kernel matrix
    #no parameters necessary, as they contained in sigma kernel!!!
    #furth try
    n = len(y)
    I_n = np.identity(n)
    I_nn = np.identity(2*n)
    H_1 = np.concatenate((I_n, -I_n), axis=1)
    H_2 = np.concatenate((I_n, I_n), axis=1)
    P = matrix(1/float(8) * np.dot( np.dot(H_1.transpose(), K[:n,:n]) , H_1))
    q = matrix((epsL*np.dot(np.ones((1,n)), H_2) - np.dot(y.reshape((1,-1)), H_1)).reshape((-1,1)))
    G = matrix(np.concatenate((I_nn, -I_nn), axis=0))
    h = matrix(np.concatenate((np.ones(2*n), np.zeros(2*n)), axis=0))
    solvers.options['show_progress'] = False
    try:
        duals = solvers.qp(P, q, G, h)['x'] #x = (\alpha_1,...,\alpha_n,\hat{\alpha}_1,...,\hat{\alpha}_n )^T
    except:
        print "Warning: qp-solver failed.",epsL,
        return np.array([]), np.array([])
    pi = 1/float(4) * np.dot(H_1, np.array(duals).reshape((-1,1))).reshape(-1) ###nu->4
    lim = 0.0000000001

    svs = []
    coeffs = []
    for i in xrange(n):
        if np.abs(pi[i]) > lim:
            svs.append(Xidx[i])
            coeffs.append(0.5*pi[i]) #final predictor is 1/2*f for sigma-cosvr
    #third try
    """
    n = len(y)
    I_n = np.identity(n)
    I_nn = np.identity(2*n)
    H_1 = np.concatenate((I_n, -I_n), axis=1)
    H_2 = np.concatenate((I_n, I_n), axis=1)
    P = matrix(1/float(8) * np.dot( np.dot(H_1.transpose(), K[:n,:n]) , H_1)) ###nu->8
    q = matrix((epsL * np.sum(H_2, axis=0) - np.dot(y.reshape(1,-1), H_1)).reshape(-1))
    G = matrix(np.concatenate((I_nn, -I_nn), axis=0))
    h = matrix(np.concatenate((np.ones(2*n), np.zeros(2*n)), axis=0))
    solvers.options['show_progress'] = False
    try:
        duals = solvers.qp(P, q, G, h)['x'] #x = (\alpha_1,...,\alpha_n,\hat{\alpha}_1,...,\hat{\alpha}_n )^T
    except:
        print "Warning: qp-solver failed.",epsL,
        return np.array([]), np.array([])
    pi = 1/float(4) * np.dot(H_1, np.array(duals).reshape(-1)) ###nu->4
    lim = 0.0000000001

    svs = []
    coeffs = []
    for i in xrange(n):
        if np.abs(pi[i]) > lim:
            svs.append(Xidx[i])
            coeffs.append(pi[i])
    """
    #second try
    """
    n = len(y)
    I_n = np.identity(n)
    I_nn = np.identity(2*n)
    H_1 = 1/float(4)*np.concatenate((I_n, -I_n), axis=1)
    H_2 = 1/float(4)*np.concatenate((I_n, I_n), axis=1)
    P = matrix(2 * np.dot( np.dot(H_1.transpose(), K[:n,:n]) , H_1)) ###nu->8
    q = matrix(4*(epsL * np.sum(H_2, axis=0) - np.dot(y.reshape(1,-1), H_1)).reshape(-1))
    G = matrix(np.concatenate((I_nn, -I_nn), axis=0))
    h = matrix(np.concatenate((np.ones(2*n), np.zeros(2*n)), axis=0))
    solvers.options['show_progress'] = False
    try:
        duals = solvers.qp(P, q, G, h)['x'] #x = (\alpha_1,...,\alpha_n,\hat{\alpha}_1,...,\hat{\alpha}_n )^T
    except:
        print "Warning: qp-solver failed.",epsL,
        return np.array([]), np.array([])
    pi = 1/float(4) * np.dot(H_1, np.array(duals).reshape(-1)) ###nu->4
    lim = 0.0000000001

    svs = []
    coeffs = []
    for i in xrange(n):
        if np.abs(pi[i]) > lim:
            svs.append(Xidx[i])
            coeffs.append(1/float(2)*pi[i]) #factor 1/float(2) arises from the final predictor being 1/2f*
    print "sigmacosvr: n",n, "n.o.svs", len(svs), float(4)*np.array(coeffs)
    """
    #first try
    """
    n = len(y)
    I_n = np.identity(n)
    I_nn = np.identity(2*n)
    H_1 = np.concatenate((I_n, -I_n), axis=1)
    H_2 = np.concatenate((I_n, I_n), axis=1)
    P = matrix(1/float(8) * np.dot( np.dot(H_1.transpose(), K[:n,:n]) , H_1)) ###nu->8
    q = matrix((epsL * np.sum(H_2, axis=0) - np.dot(y.reshape(1,-1), H_1)).reshape(-1))
    G = matrix(np.concatenate((I_nn, -I_nn), axis=0))
    h = matrix(np.concatenate((np.ones(2*n), np.zeros(2*n)), axis=0))
    solvers.options['show_progress'] = False
    try:
        duals = solvers.qp(P, q, G, h)['x'] #x = (\alpha_1,...,\alpha_n,\hat{\alpha}_1,...,\hat{\alpha}_n )^T
    except:
        print "Warning: qp-solver failed.",epsL,
        return np.array([]), np.array([])
    pi = 1/float(4) * np.dot(H_1, np.array(duals).reshape(-1)) ###nu->4
    lim = 0.0000000001

    svs = []
    coeffs = []
    for i in xrange(n):
        if np.abs(pi[i]) > lim:
            svs.append(Xidx[i])
            coeffs.append(pi[i])
    """

    return np.array([svs]), np.array([coeffs])

def toytest_svr_train(K, Xidx, y, nu, epsL):
    #K...kernel matrix over labelled examples, K = original gram matrix + np.ones((n,n))
    #L...indices of the data rows in the data object
    #y...vector of labels, 1-dimensional np.array of length n
    #nu...norm-regularization parameter
    #epsL...margin parameter of epsilon-insensitive loss
    n = len(y) #number of training examples
    I_n = np.identity(n)
    I_nn = np.identity(2*n)
    H_1 = np.concatenate((I_n, -I_n), axis=1)
    H_2 = np.concatenate((I_n, I_n), axis=1)
    P = matrix(1/float(nu) * np.dot( np.dot(H_1.transpose(), K) , H_1))
    q = matrix((epsL * np.sum(H_2, axis=0) - np.dot(y.reshape(1,-1), H_1)).reshape(-1))
    G = matrix(np.concatenate((I_nn, -I_nn), axis=0))
    h = matrix(np.concatenate((np.ones(2*n), np.zeros(2*n)), axis=0))
    solvers.options['show_progress'] = False
    duals = solvers.qp(P, q, G, h)['x'] #x = (\alpha_1,...,\alpha_n,\hat{\alpha}_1,...,\hat{\alpha}_n )^T
    #print "duals", duals
    #print "nu", nu
    pi = 1/float(nu) * np.dot(H_1, np.array(duals).reshape(-1)) #calculation of kernel expansion \pi via alpha-variables
    """
    lim = 0.00001 #tolerance parameter, very small entries are assumed to be zero
    #print "pi",pi
    svs = []
    coeffs = []
    for i in xrange(n):
        if np.abs(pi[i]) > lim:
            svs.append(Xidx[i])
            coeffs.append(pi[i])
    """
    svs = []
    coeffs = []
    for i in xrange(n):
        if pi[i] != 0.0:
            svs.append(Xidx[i])
            coeffs.append(pi[i])
    #svs...list of support vector indices
    #coeffs...list of support vector coefficients
    print "svr: n",n, "svs", svs, float(nu)*np.array(coeffs) #kernel expansion coefficients, not equal to dual variables
    return np.array(svs), np.array(coeffs)

if __name__ == "__main__":
    #def toytest_svr_train(K, Xidx, y, nu, epsL):
    #def eps_cosvr_train(Ks, Xidx, y, epsL, epsU, nus, lam):
    import math
    #X1 = np.array([[1,1],[2,2],[3,3],[4,4],[5,5],[10,4],[11,5],[12,6],[13,7],[14,8]]).astype(float)
    X1 = np.array([[1,1],[2,2],[3,3],[4,4],[5,5],[10,4],[11,5],[12,6],[13,7],[14,8]]).astype(float)
    #X2 = np.array([[1,1],[2,2],[3,3],[4,4],[5,5],[4,7],[5,8],[6,9],[7,10],[8,11]]).astype(float)
    X2 = np.array([[1,1],[2,2],[3,3],[4,4],[5,5],[4,-2],[5,-1],[6,0],[7,1],[8,2]]).astype(float)
    n = 5
    m = 5
    y = np.array([1,2,3,4,5])
    y_unlab = np.array([6,7,8,9,10])
    Xidx = range(10)
    K1 = np.dot(X1, X1.transpose())
    K2 = np.dot(X2, X2.transpose())
    svs1, coeffs1 = toytest_svr_train(K1[:n,:n], Xidx, y, 1.0, 0.1)
    svs2, coeffs2 = toytest_svr_train(K2[:n,:n], Xidx, y, 1.0, 0.1)
    svs_co, coeffs_co = eps_cosvr_train([K1,K2], Xidx, y, 0.01,0.01,[0.01,0.01],100.0)
    linmodel1 = np.dot(coeffs1.reshape((1,-1)), X1[svs1,:])
    linmodel2 = np.dot(coeffs2.reshape((1,-1)), X2[svs2,:])
    linmodel1_cosvr = np.dot(coeffs_co[0].reshape((1,-1)), X1[svs_co[0],:])
    linmodel2_cosvr = np.dot(coeffs_co[1].reshape((1,-1)), X2[svs_co[1],:])
    pred1 = np.dot(K1[m:,svs1], coeffs1.reshape((-1,1))).reshape(-1)
    pred2 = np.dot(K2[m:,svs2], coeffs2.reshape((-1,1))).reshape(-1)
    pred_cosvr = 0.5*(np.dot(K1[m:,svs_co[0]], coeffs_co[0].reshape((-1,1))).reshape(-1) + np.dot(K2[m:,svs_co[1]], coeffs_co[1].reshape((-1,1))).reshape(-1) )
    rmse1 = 1/float(math.sqrt(m))*np.linalg.norm(pred1.reshape(-1)-y_unlab.reshape(-1))  
    rmse2 = 1/float(math.sqrt(m))*np.linalg.norm(pred2.reshape(-1)-y_unlab.reshape(-1))  
    rmse_cosvr = 1/float(math.sqrt(m))*np.linalg.norm(pred_cosvr.reshape(-1)-y_unlab.reshape(-1))  
    print "svr 1: pred1:", pred1, "rmse1:", rmse1, "linmodel1:", linmodel1
    print "svr 2: pred2:", pred2, "rmse2:", rmse2, "linmodel2:", linmodel2
    print "cosvr: pred_cosvr:", pred_cosvr, "rmse_cosvr:", rmse_cosvr
    print "linmodel1_cosvr", linmodel1_cosvr, "linmodel2_cosvr:", linmodel2_cosvr
    
    
    
  
