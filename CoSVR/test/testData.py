'''
Created on 29.04.2015

@author: Michael Kamp
'''

import unittest
from data.dataHandler import DataHandler

UCI_DATASET_COUNT = 32
LIGANDS_DATASET_COUNT = 29

class TestData(unittest.TestCase):    
    def testDataReading(self):
        dataHandler = DataHandler(normalize = True)
        UCIdatasets = dataHandler.getUCIDatasets()#
        LigandsDatasets = dataHandler.getLigandsDatasets()        
        data = dataHandler.getData('autoHorse', dataHandler.UCI)        
        testStringAutHorse = "autoHorse(UCI) (159,58), v: 0 l:0 u:0"
        self.assertEqual(str(data), testStringAutHorse, "Data seems to be wrong: "+str(data)+"\n It should have been: "+testStringAutHorse)
        self.assertEqual(len(UCIdatasets), UCI_DATASET_COUNT, "Number of UCI datasets is unexpected. "+str(UCI_DATASET_COUNT)+" expected, but " +str(len(UCIdatasets))+ " found.")
        self.assertEqual(len(LigandsDatasets), LIGANDS_DATASET_COUNT, "Number of ligands datasets is unexpected. "+str(LIGANDS_DATASET_COUNT)+" expected, but " +str(len(LigandsDatasets))+ " found.")
        dataUCI = dataHandler.getData(UCIdatasets[0])
        dataLIG = dataHandler.getData(LigandsDatasets[0])
        self.assertEqual(dataUCI.name, 'autoHorse', "Unexpected dataset. Expected \'autoHorse\' but got \'"+dataUCI.name+"\'.")
        self.assertEqual(dataUCI.getDimension(),58,"Unexpected number of dimensions. Expected 58 but got "+str(dataUCI.getDimension()))
        self.assertEqual(len(dataUCI.views),0,"Unexpected number of views. Expected 0 but got "+str(len(dataUCI.views)))
        self.assertEqual(dataUCI.sX.shape,(159,58),"Unexpected data shape. Expected (159,58) but got "+str(dataUCI.sX.shape))
        self.assertEqual(dataLIG.name, 'P03952', "Unexpected dataset. Expected \'P03952\' but got \'"+dataLIG.name+"\'.")
        self.assertEqual(dataLIG.getDimension(),8752,"Unexpected number of dimensions. Expected 8752 but got "+str(dataLIG.getDimension()))
        self.assertEqual(len(dataLIG.views),10,"Unexpected number of views. Expected 10 but got "+str(len(dataLIG.views)))
        self.assertEqual(dataLIG.sX.shape,(76, 8752),"Unexpected data shape. Expected (76, 8752) but got "+str(dataLIG.sX.shape))