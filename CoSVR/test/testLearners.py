'''
Created on 24.04.2015

@author: Michael Kamp
'''

import unittest
import numpy as np
from sklearn import svm
from learner.kernel import GaussKernel, LinearKernel, Kernel
from learner.svr import svr_train
from learner.rlsr import rlsr_train, rlsr_predict
import math

class TestLearners(unittest.TestCase):
    def testKernelBaseClass(self):
        dim = 4
        nExX = 5
        nExY = 3
        X = np.zeros((nExX,dim))
        Y = np.zeros((nExY,dim))
        kernel = Kernel()
        K = kernel.computeKernelMatrix(X,Y)
        self.assertEqual(K.shape, (nExX,nExY), "Kernel Base Class failed. Kernel should have shape "+str((nExX,nExY))+" but has shape "+str(K.shape))
    
    def testGaussKernel(self):
        kernel = GaussKernel(sigma = 1.)
        X = np.array([[1,1,1],[1,1,2]])
        Y = np.array([[1,1,1],[1,1,2]])
        K = kernel.computeKernelMatrix(X,Y)
        Ktrue = [[kernel.computeKernelValue(X[0],Y[0]),kernel.computeKernelValue(X[0],Y[1])],[kernel.computeKernelValue(X[1],Y[0]),kernel.computeKernelValue(X[1],Y[1])]]
        self.assert_(np.array_equal(K, Ktrue), "Gaussian Kernel failed. Result should have been " + str(Ktrue) + " but was "+str(K))
        
    def testLinearKernel(self):
        kernel = LinearKernel()
        X = np.array([[1,1,1],[1,1,2]])
        Y = np.array([[1,1,1],[1,1,2]])
        K = kernel.computeKernelMatrix(X,Y)
        Ktrue = [[kernel.computeKernelValue(X[0],Y[0]),kernel.computeKernelValue(X[0],Y[1])],[kernel.computeKernelValue(X[1],Y[0]),kernel.computeKernelValue(X[1],Y[1])]]
        self.assert_(np.array_equal(K, Ktrue), "Linear Kernel failed. Result should have been " + str(Ktrue) + " but was "+str(K))

    def testSVR_Train(self):
        n=80
        m=20
        d=10
        #X = random.uniform(0.0,10.0,((n+m),d))
        np.random.seed(134765)
        X = np.random.standard_normal(((n+m), d))
        K = np.dot(X,X.transpose()) + np.ones((n+m,n+m))
        combi = X[0] + X[1]
        shift = 0.0
        y = np.dot(X, combi.reshape(-1,1)) + shift*np.ones((n+m,1))
        eps = 3.0
        nu = 0.5
    
        #sklearn svr
        f = svm.SVR(kernel='precomputed', C=1/float(nu), epsilon=eps)
        f.fit(K[:n,:n], y[:n].reshape(-1))
        svs1 = f.support_
        coeffs1 = f.dual_coef_
        b1 = f.intercept_
        pred1 = np.dot(K[n:,svs1], coeffs1.reshape(-1,1)) + b1*np.ones((m,1))
        rmse1 = np.linalg.norm(pred1-y[n:])
    
        #our svr
        svs2, coeffs2 = svr_train(K[:n,:n], range(len(X)), y.reshape(-1)[:n], nu, eps)    
        pred2 = np.dot(K[n:,svs2], coeffs2.reshape(-1,1))
        rmse2 = np.linalg.norm(pred2-y[n:])
            
        diffCoeffs = np.linalg.norm(coeffs1-coeffs2)
        diffPreds = np.linalg.norm(pred1-pred2)
        diffRMSE = np.abs(rmse1 - rmse2)
        
        eps = 0.05
        
        self.assert_(set(svs1) == set(svs2), "SVR training failed. Support vectors are unequal to SKLEARN ones. Our SVs:\n" + str(svs2) + "\nSKLEARN SVs:\n "+str(svs1))
        self.assert_(diffCoeffs < eps, "SVR training failed. Learned coefficiants are unequal. Difference " + str(diffCoeffs) + " is larger than threshold "+str(eps)+".")
        self.assert_(diffPreds < eps, "SVR training failed. Predictions are unequal. Difference " + str(diffPreds) + " is larger than threshold "+str(eps)+".")
        self.assert_(diffRMSE < eps, "SVR training failed. RMSEs are unequal. Difference " + str(diffRMSE) + " is larger than threshold "+str(eps)+".")
        
    def testRLSR_train(self):
        n=80
        m=20
        d=10
        np.random.seed(134765)
        X = np.random.standard_normal(((n+m), d))
        K = np.dot(X,X.transpose()) + np.ones((n+m,n+m))
        combi = X[0] + X[1]
        shift = 10.0
        y = np.dot(X, combi.reshape(-1,1)) + shift*np.ones((n+m,1))
        nu = 0.01
        
        svs, coeffs = rlsr_train(K, range(len(X)), y, nu)
        K_pred = K[n:]
        y_pred = rlsr_predict(K_pred, coeffs)
        rmse = 1/float(math.sqrt(m))*np.linalg.norm(y_pred.reshape(-1)-y[n:].reshape(-1))
       
        eps = 0.01
        self.assert_(rmse < eps, "Kernel RLSR training failed. RMSE should be below "+str(eps)+"but is acutally "+str(rmse)+".")
