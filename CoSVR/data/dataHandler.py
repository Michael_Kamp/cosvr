'''
Created on 23.04.2015

@author: Michael Kamp
'''

import os
import copy
import collections as cl
import numpy as np
import importlib
import ast

LIGANDS_PATH = "LIGANDS/csv/"
UCI_PATH = "UCI/csv/"
TOYDATA_PATH = "TOYDATA/"
UCI = 'UCI'
LIGANDS = 'ligands'
TOYDATA = 'toydata'

class DataHandler():    
    def __init__(self, views = [], normalize = False):
        self.views = views
        self.datapath = os.path.dirname(os.path.abspath(__file__))+"/"
        self.datasets = cl.defaultdict(set)    
        self.datasetInfo = cl.defaultdict(lambda : cl.defaultdict(lambda : cl.defaultdict(set)))
        self.readDatasetNames()
        self.UCI = UCI
        self.LIGANDS = LIGANDS        
        self.TOYDATA = TOYDATA
        self.bNormalize = normalize
        
    #acInfo = [['O60235', 41], ['P00734', 2648], ['P00740', 171], ['P00742', 2626], ['P00747', 474], 
    #            ['P00749', 600], ['P00750', 264], ['P03952', 76], ['P04070', 31], ['P07288', 28], 
    #            ['P07339', 197], ['P07384', 189], ['P07477', 986], ['P07711', 357], ['P07858', 278], 
    #            ['P08246', 742], ['P08311', 23], ['P08709', 249], ['P09871', 92], ['P14091', 21], 
    #            ['P17655', 128], ['P23946', 90], ['P25774', 104], ['P29466', 310], ['P42574', 133], 
    #            ['P43235', 252], ['Q16651', 23], ['Q99895', 91], ['Q9Y5Y6', 125]]   
    #sortedNamesList = ['P14091', 'P08311', 'Q16651', 'P07288', 'P04070', 'O60235', 'P03952', 'P23946', 
    #                   'Q99895', 'P09871', 'P25774', 'Q9Y5Y6', 'P17655', 'P42574', 'P00740', 'P07384', 
    #                   'P07339', 'P08709', 'P43235', 'P00750', 'P07858', 'P29466', 'P07711', 'P00747', 
    #                   'P00749', 'P08246', 'P07477', 'P00742', 'P00734']
    def readDatasetNames(self):
        #read ligand file names
        ligand_views =[name for name in os.listdir(self.datapath+LIGANDS_PATH) if os.path.isdir(os.path.join(self.datapath+LIGANDS_PATH, name))] 
        for view in ligand_views:
            if self.views == [] or view in self.views:
                #for name in ['P14091','P08311','Q16651','P07288','P04070','O60235','P23946','Q99895','P09871']:
                #for name in ['P23946','Q99895','P09871','P25774','Q9Y5Y6','P17655']:
                #for name in ['P14091', 'P08311', 'Q16651', 'P07288', 'P04070', 'O60235', 'P03952', 'P23946', 
                #             'Q99895', 'P09871', 'P25774', 'Q9Y5Y6', 'P17655', 'P42574', 'P00740', 'P07384']:
                for stFile in os.listdir(self.datapath+LIGANDS_PATH+view+"/"):
                    name = stFile.replace("_"+view,"").replace(".csv", "")
                    self.datasets[LIGANDS].add(name)
                    self.datasetInfo[LIGANDS][name]['views'].add(view)
        for stFile in os.listdir(self.datapath+UCI_PATH):
            name = stFile.replace("_mod.csv", "")
            self.datasets[UCI].add(name)
        for stFile in os.listdir(self.datapath+TOYDATA_PATH):
            if ".pyc" in stFile:
                continue            
            name = stFile.replace(".py", "")
            if not "__init__" in name:
                self.datasets[TOYDATA].add(name)
        #self.datasets[UCI] = ["sleep","echoMonths","pollution","fishcatch","pyrim","auto93","baskball","cloud",
        #                      "fruitfly","veteran","autoPrice","auto_price","autoHorse","servo","triazines","lowbwt",
        #                      "pharynx","wisconsin","pwLinear","machine_cpu","cpu","bodyfat","meta","pbc","breastTumor",
        #                      "cholesterol","cleveland","autoMpg","housing","sensory","strike","stock"]
    
    def orderDatasetsBySize(self, datasets):
        sizes = {}
        for name in datasets:
            print "name", name
            size = 0.0
            if name in self.datasets[UCI]:                
                assert name not in self.datasets[LIGANDS], "Type is none and dataset name is ambiguous."
                size = os.path.getsize(self.datapath+UCI_PATH + name + "_mod.csv")
            elif name in self.datasets[LIGANDS]:
                for view in self.datasetInfo[LIGANDS][name]['views']:
                    size += os.path.getsize(self.datapath+LIGANDS_PATH + view + "/" + name + "_" + view + ".csv")
            sizes[name] = size
        sorted_datasets = [k for (k,v) in sorted(sizes.items(), key = lambda (k,v): v)]
        return sorted_datasets
    
    def orderDatasetsByNumberOfExamples(self, datasets):
        sizes = {}
        for name in datasets:
            size = 0.0
            if name in self.datasets[UCI]:                
                assert name not in self.datasets[LIGANDS], "Type is none and dataset name is ambiguous."
                size = len(open(self.datapath+UCI_PATH + name + "_mod.csv",'r').readlines())
            elif name in self.datasets[LIGANDS]:
                view = list(self.datasetInfo[LIGANDS][name]['views'])[0] #all views of a dataset have the same number of examples
                size = len(open(self.datapath+LIGANDS_PATH + view + "/" + name + "_" + view + ".csv").readlines())
            sizes[name] = size
        sorted_datasets = [k for (k,v) in sorted(sizes.items(), key = lambda (k,v): v)]
        return sorted_datasets
        
    def getData(self, name, dtype = None):
        X,Y = np.array([]), np.array([])
        if dtype == None:
            if name in self.datasets[UCI]:                
                dtype = UCI
                assert name not in self.datasets[LIGANDS], "Type is none and dataset name is ambiguous."                
            elif name in self.datasets[LIGANDS]:
                dtype = LIGANDS  
            elif name in self.datasets[TOYDATA] or any(s in name for s in self.datasets[TOYDATA]):
                dtype = TOYDATA                                  
        if dtype == UCI:
            X, Y, views = self.readUCI(name)
        elif dtype == LIGANDS:
            X, Y, views = self.readLIGANDS(name)
        elif dtype == TOYDATA:
            X, Y, views = self.getToyData(name)
        else:
            assert False, "Error: wrong data type. Should be in "+str([UCI,LIGANDS])+" but was "+str(dtype)
        #normalize labels to zero mean and unit variance
        if self.bNormalize:
            X, Y = self.normalize(X, Y)
        data = Data(X, Y, name, dtype, copy.deepcopy(self.datasetInfo[dtype][name]), views) #datasetInfo is deppcopied so that the data object can update info, e.g., for creating new views
        return data
    
    def getDatasets(self, dtype):
        return list(self.datasets[dtype])
    
    def getUCIDatasets(self):
        return list(self.datasets[UCI])
    
    def getLigandsDatasets(self):
        return list(self.datasets[LIGANDS])
    
    def getToyDatasets(self):
        return list(self.datasets[TOYDATA])
        
    def readUCI(self, name):
        #name: dataset name
        #UCI: 32 one-view datasets, real-valued descriptors
        assert name in self.datasets[UCI]
        inFile = open(self.datapath+UCI_PATH + name + "_mod.csv", "r").readlines()
        firstLine = inFile.pop(0)
        height = int(firstLine.rstrip().split(" ")[1])
        width = int(firstLine.rstrip().split(" ")[3])
        X = np.zeros((height, width))
        Y = np.zeros(height)
    
        for line, h in zip(inFile, xrange(height)):
            featureList = (line.rstrip()).split(",")
            label = featureList.pop(-1)
            Y[h] = float(label)
            for w in xrange(width):
                X[h,w] = float(featureList[w]) #real values
        #X: 2-dimensional array: "instance" x "descriptor"
        #Y: 1-dimensional array: label
        self.datasetInfo[UCI][name]['instances'] = X.shape[0]
        self.datasetInfo[UCI][name]['dimension'] = X.shape[1]        
        return X, Y, []
    
    def readLIGANDS(self,  name):
        #name: protein name
        #LIGANDS: 29 multi-view datasets, 10 views, binary descriptors
        assert name in self.datasets[LIGANDS]        
        views = ["ecfp2", "ecfp4", "ecfp6", "fcfp2", "fcfp4", "fcfp6", "gpidaph3", "maccs", "tgd", "tgt"]
        if self.views != []:
            views = self.views
        Xs = []
        for view,v in zip(views, xrange(len(views))):
            inFile = open(self.datapath+LIGANDS_PATH + view + "/" + name + "_" + view + ".csv", "r").readlines()
            firstLine = inFile.pop(0)
            width = int((firstLine.rstrip().split(" ")[-2]).split(",")[0]) #width dependent on name(!) and view
            if v == 0: #height independent of view
                height = len(inFile)
                Y = np.zeros(height) #Y independent of view
            Xv = np.zeros((height, width))
    
            for line, h in zip(inFile, xrange(height)):
                featureList = (line.rstrip()).split(",")
                label = featureList.pop(-1) #label popped
                if v == 0:
                    Y[h] = float(label) #Y needs to be generated only once
                for feature in featureList:
                    Xv[h, int(feature)-1] = 1 #found feature positions set to 1
            Xs.append(Xv)
            #X: list of 2-dimensional arrays, elements belong to views: "instance" x "view descriptor"
            #Y: 1-dimensional array: label
        X = np.concatenate(tuple(Xs), axis = 1)
        views = []     
        curPos = 0.   
        for x in Xs:
            v = np.add(np.arange(x.shape[1]), curPos)
            curPos += float(x.shape[1])
            views.append(v.astype(int))
        self.datasetInfo[LIGANDS][name]['instances'] = X.shape[0]
        self.datasetInfo[LIGANDS][name]['dimension'] = X.shape[1]   
        return X, Y, views
    
    def preprocessToydataName(self, name):
        kwargs = {}
        if '(' in name and ')' in name:
            args = name[name.index('(')+1:name.index(')')]
            name = name[:name.index('(')]
            kwargs = ast.literal_eval(args)
        return name, kwargs
    
    def getToyData(self, name):               
        name, kwargs = self.preprocessToydataName(name)
        toyDataModule = importlib.import_module("data.TOYDATA."+name)      
        X, Y, views = toyDataModule.getData(**kwargs)
        self.datasetInfo[LIGANDS][name]['instances'] = X.shape[0]
        self.datasetInfo[LIGANDS][name]['dimension'] = X.shape[1]   
        self.datasetInfo[LIGANDS][name]['views'] = ["view"+str(i+1) for i in xrange(len(views))]
        return X, Y, views
     
    def normalize(self, X, y):        
#         avgY = np.average(y)
#         varY = np.var(y)
#         y_new = (y - avgY) / varY
#         assert(y_new.shape == y.shape)
        y_new = y
        #normalize views column-wize to zero mean and unit variance
        X_new = np.zeros(X.shape)
        avgX = np.average(X, axis = 0) #vector of column-wize averages
        varX = np.var(X, axis = 0) #vector of column-wize averages
        for col in xrange(len(avgX)):
            if varX[col] != 0:
                X_new[:,col] = (X[:,col] - avgX[col]) / varX[col]
            else:
                X_new[:,col] = (X[:,col] - avgX[col])
        assert(X_new.shape == X.shape)
        return X_new, y_new
        
class Data():
    def __init__(self, X, Y, name, dtype, dataInfo, views = None, L = [], U = []):
        self.sX = X
        self.sY  = np.array(Y)                
        if views == None:
            views = np.arange(len(X))
        self.views = [v.astype(int) for v in views]
        self.iL = np.array(L)
        self.iU = np.array(U)
        self.iYL = np.array(L)
        self.iYU = np.array(U)
        self.name = name
        self.type = dtype
        self.dataInfo = dataInfo
        self.updateInfo()
        self.kernels = {}

    def getTrainTestSplit(self, Xidx, L_ftrain = 0.1, U_ftrain = 0.9, U_ftest = 0.0, numLfix = None):
        #assert float(L_ftrain + U_ftrain + U_ftest) == 1, "Fractions of training and test data do not sum up to one: "+str(L_ftrain)+" "+str(U_ftrain)+" "+str(U_ftest)
        np.random.shuffle(Xidx)
        n = len(Xidx)
        nltrain = int(L_ftrain*n)        
        if nltrain < 2: #at least 2 so that for param evaluation there ist at least 1 labeled and 1 test example
            nltrain = 2
        nutrain = int(U_ftrain*n)
        nutest = n - nltrain - nutrain
        if U_ftest == 0.0:
            nutrain += nutest
            nutest = 0
        if numLfix != None:
            nltrain = numLfix
            if nltrain < 2:
                nltrain = 2
            nutest = int(U_ftest*n)
            nutrain = n - nltrain - nutest            
        assert np.abs(nutest - int(U_ftest*n)) <= 2, "Error: splitting into train and test went wrong. Size of test set is unexpected."
        L_train = Xidx[:nltrain]
        U_train = Xidx[nltrain:nutrain+nltrain]
        U_test  = Xidx[nutrain+nltrain:]
        L = L_train
        U = np.append(U_train,U_test)
        self.setL(L)
        self.setU(U)
        return L_train, U_train, U_test
                            
    def setL(self, L):
        self.iL = np.array(L)
    
    def setU(self, U):
        self.iU = np.array(U)
            
    def getLidx(self):      
        return self.iL
    
    def getUidx(self):
        return self.iU
    
    def getXidx(self):
        return np.array(range(len(self.sX)))
    
    def getLenL(self):
        return len(self.iL)
    
    def getLenU(self):
        return len(self.iU)
    
    def getData_(self, idx = None, vidx = None, labeled = None):
        if labeled == True:
            assert len(idx) == len(set(idx)&set(self.iL)), "Error: indices are not from labeled data: "+str(set(idx)-set(self.iL))
        if labeled == False:
            assert len(idx) == len(set(idx)&set(self.iU)), "Error: indices are not from labeled data: "+str(set(idx)-set(self.iU))
        if idx == None and vidx == None: #return complete data for all views
            return self.sX
        elif idx == None: #return complete data for a specific view v
            return self.sX[:,self.views[vidx]]
        elif vidx == None: #return specific data (rows specified in idx) for all views
            return self.sX[idx]
        else: #return specific data (rows specified in idx) for a specific view v
            v = self.views[vidx].astype(int)
            return self.sX[idx][:,v]  
    
    def getX(self, idx = None, vidx = None):
        return self.getData_(idx, vidx, None)
        
    def getL(self, idx = None, vidx = None):
        if idx == None:
            idx = self.iL
        return self.getData_(idx, vidx, True)        
    
    def getU(self, idx = None, vidx = None):
        if idx == None:
            idx = self.iU
        return self.getData_(idx, vidx, False)
    
    def getY(self, idx = None):
        if idx == None:            
            return self.sY
        else:            
            return self.sY[idx]
        
    def getYL(self, idx = None):
        if idx == None:
            return self.sY[self.iL]
        else:
            assert len(idx) == len(set(idx)&set(self.iL)), "Error: indices are not from labeled data: "+str(set(idx)-set(self.iL))
            return self.sY[idx]
            
    def getYU(self, idx = None):
        if idx == None:
            return self.sY[self.iU]
        else:
            assert len(idx) == len(set(idx)&set(self.iU)), "Error: indices are not from labeled data: "+str(set(idx)-set(self.iU))
            return self.sY[idx]
        
    def __getitem__(self, pos):
        if len(pos) != 2:
            print "Error: wrong index. Please provide: view, index as tuple. Provided was: ",pos
            return None
        v, idx = pos
        return self.sX[v][idx], self.sY[idx]
    
    def __len__(self):
        return len(self.sX)
    
    def getDimension(self):
        if 'dimension' not in self.dataInfo.keys():
            if len(self.views) == 0:                 
                self.dataInfo['dimension'] = self.sX.shape[1]
            else:
                self.dataInfo['dimension'] = 0
                for view in self.views:
                    self.dataInfo['dimension'] += len(view)
        return self.dataInfo['dimension']
    
    def setViews(self, views):
        self.views = [copy.deepcopy(v).astype(int) for v in views]
        self.updateInfo()
    
    def getViewIndex(self, v):
        vidx = 0
        while vidx < len(self.views):
            if np.array_equal(self.views[vidx],v):
                return vidx
            vidx += 1
        return -1
                    
    def updateInfo(self):
        self.dataInfo['name'] = self.name
        self.dataInfo['type'] = self.type
        self.dataInfo['instances'] = len(self.sX)
        self.dataInfo['number_views'] = len(self.views)
        self.dataInfo['labeled'] = len(self.iL)
        self.dataInfo['unlabeled'] = len(self.iU)
        if len(self.views) == 0:                 
            self.dataInfo['dimension'] = self.sX.shape[1]
        else:
            self.dataInfo['dimension'] = 0
            for view in self.views:
                self.dataInfo['dimension'] += len(view)
                
    def getInfo(self):
        self.updateInfo()
        stInfo = "("+str(self.dataInfo['instances'])+","+str(self.dataInfo['dimension'])+"), v: "+str(self.dataInfo['number_views'])+" l:"+str(self.dataInfo['labeled']) + " u:"+str(self.dataInfo['unlabeled'])
        return stInfo
    
    def generateViewCombinations(self, number_of_views, numberViewCombinations):
        colIdx = np.array(range(self.getDimension()))
        viewCombinations = []
        for i in xrange(numberViewCombinations):
            np.random.shuffle(colIdx)
            views = np.array_split(colIdx, number_of_views)     
            viewCombinations.append(views)
        #print "viewCombinations", viewCombinations #indices in sX : [[combi1_view1, combi1_view2], ...]
        return viewCombinations
        
    def hasPrecomputedKernel(self, k, vidx):
        if k in self.kernels:
            v = tuple(self.views[vidx].tolist())
            if v in self.kernels[k]:
                return True
        return False
                
    def getPrecomputedKernel(self, k, vidx):
        if k in self.kernels:
            v = tuple(self.views[vidx].tolist())
            if v in self.kernels[k]:
                return self.kernels[k][v]
        return None
    
    def __str__(self):
        return str(self.name) + "("+str(self.type)+")"+" " + self.getInfo()