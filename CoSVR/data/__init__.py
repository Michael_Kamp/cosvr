'''
Created on 16.04.2015

@author: Michael Kamp
'''

import os
import collections as cl
import numpy as np

LIGANDS_PATH = "LIGANDS/csv/"
UCI_PATH = "UCI/csv/"

class DataHandler():
    def __init__(self):
        self.datapath = os.path.dirname(os.path.abspath(__file__))+"/"
        self.datasets = cl.defaultdict(list)    
        self.datasetInfo = {}
        
class Data():
    def __init__(self, L, U, Y, dataInfo):
        self.sL = L
        self.sU = U
        self.sY = Y
        self.dataInfo = dataInfo        
        self.views = np.array(range(len(self.sL)))
            
    def getL(self):
        if not hasattr(self, 'L'):            
            if len(self.sL.shape) == 3: #Labeled examples are available in multiple views
                L = [[]]*len(self.sL)
                for v in xrange(len(self.sL)):
                    L[v] = range(len(self.sL[v]))
                self.L = np.array(L)    
            else:
                print "Error: shape of labelled data is wrong: ", str(self.sL.shape)          
        return self.L
        