#!/usr/bin/python

import numpy as np
import re

def targetList(path):
    targets = open(path + "proteins.txt").readlines()
    firstLine = targets.pop(0)
    nameList = []
    for line in targets:
        name = (line.split("\t")[0]).rstrip()
        nameList.append(name)
    nameList.sort()
    return nameList

def genDictionary(ta, fp, path):
    inFile = open(path + "fp_rawdata/" + fp + "/" + ta + "." + fp + ".txt", "r").readlines()
    firstLine = inFile.pop(0)
    dic = {}
    item = 0 #feature id in dictionary
    for line in inFile:
        featureLine = re.split("\s+", line.rstrip())
        compoundID = featureLine.pop(0) #compound id
        
        for feature in featureLine:
            if feature not in dic.iterkeys():
                item += 1
                dic[feature] = item
    #print dic
    return dic, item #last feature item equals n.o. features
 
def renumber(ta, fp, path, dic, maxFeature):

    inFileCompounds = open(path + "fp_rawdata/" + fp + "/" + ta + "." + fp + ".txt", "r").readlines() 
    inFilePotencies = open(path + "activities/" + ta + ".pKi.txt", "r").readlines() 
    firstLine1 = inFileCompounds.pop(0) #first line info
    firstLine2 = inFilePotencies.pop(0)

    outFile = open(path + "csv/" + fp + "/" + ta + "_" + fp + ".csv", "w")
    firstLine = "feature 1, ..., feature " + str(maxFeature) + ", activity\n"
    outFile.write(firstLine)

    for (potLine, featLine) in zip(inFilePotencies, inFileCompounds):
        splitPotLine = re.split("\s+", potLine.rstrip())
        splitFeatLine = re.split("\s+", featLine.rstrip())

        if splitPotLine[0] == splitFeatLine[0]: #identifiers match!
            compoundID = splitFeatLine.pop(0) #id popped
            stringOUT = "" #line in CSV-format ends with potency
            for feature in splitFeatLine:
                number = dic[feature]
                stringOUT += str(number) + ","
            stringOUT += splitPotLine[1] + "\n"
        else:
            print "identifiers don't match!"
        outFile.write(stringOUT)
    outFile.close()

def fp2svm(ta, fp, path, dic, maxFeature):
    inFileCompounds = open(path + "fp_rawdata/" + fp + "/" + ta + "." + fp + ".txt", "r").readlines() 
    inFilePotencies = open(path + "activities/" + ta + ".pKi.txt", "r").readlines() 
    firstLine1 = inFileCompounds.pop(0) #first line info
    firstLine2 = inFilePotencies.pop(0)

    outFile = open(path + "svm/" + fp + "/" + ta + "_" + fp + "_svm.txt", "w")

    for (potLine, featLine) in zip(inFilePotencies, inFileCompounds):
        splitPotLine = re.split("\s+", potLine.rstrip())
        splitFeatLine = re.split("\s+", featLine.rstrip())

        if splitPotLine[0] == splitFeatLine[0]: #identifiers match!
            compoundID = splitFeatLine.pop(0) #id popped
            stringOUT = splitPotLine[1] #line in CSV-format ends with potency
            #for feature in splitFeatLine:
            #    number = dic[feature]
            #    stringOUT = stringOUT + " " + str(number) + ":1"
            #stringOUT = stringOUT + "\n"
            featuresInLine = []
            for feature in splitFeatLine:
                number = dic[feature]
                featuresInLine.append(number)
            featuresInLine.sort()
            for f in featuresInLine:
                stringOUT = stringOUT + " " + str(f) + ":1"
            stringOUT = stringOUT + "\n"
        else:
            print "identifiers don't match!"
        outFile.write(stringOUT)
    outFile.close()

def main():
    path = "/home/katrin/iaisvn/camlstudents/KatrinMichael/CoSVR/data/LIGANDS/"
    #fingerprints = ["ecfp2", "ecfp4", "ecfp6", "fcfp2", "fcfp4", "fcfp6", "gpidaph3", "maccs", "tgd", "tgt"]
    fingerprints = ["gpidaph3"]   
    #fingerprints = [] 
    targets = targetList(path) #list of 29 protein targets
    #targets = ["O60235"]
    for fp in fingerprints:
        print fp
        for ta in targets:
            print ta
            dic, maxFeature = genDictionary(ta, fp, path)
            #renumber(ta, fp, path, dic, maxFeature)
            fp2svm(ta, fp, path, dic, maxFeature)

if __name__ == "__main__":
    main()

