'''
Created on 16.04.2015

@author: Michael Kamp
'''

import sys
sys.path.append("../../../../")

import collections as cl
from data.dataHandler import DataHandler
from learner.kernel import LinearKernel, BiasedLinearKernel
from learner.baselines import ConcatSVR, ConcatRLSR, SingleSVR, SingleSVR , SingleRLSR, MeanSVR, MeanRLSR
from learner.cosvr import SigmaCoSVR, EpsRelCoSVR, EpsCoSVR, L2RelCoSVR, L2CoSVR, EpsCoSVR, EpsRelCoSVR, L2CoSVR, L2RelCoSVR
from learner.corlsr import CoRLSR
from learner.paramHeuristics import NoDataDependentFactor, CoRLSR_PaperHeuristic
from framework.experiment import Experiment
from framework.evaluation import RMSE
from framework.parameterEvaluation import ParamEval

views = ['maccs', 'gpidaph3']
folds = 5

paramRanges = cl.defaultdict(list)
paramRanges['k'] = [BiasedLinearKernel()]
paramRanges['nu'] = [10**x for x in xrange(-5,3)]
paramRanges['epsU'] = [2**x for x in xrange(-4,0)]
paramRanges['epsL'] = [2**x for x in xrange(-4,0)]
paramRanges['lmbda'] = [10**x for x in xrange(-2,4)]
params = {'paramRange':paramRanges, 'paramFactorHeuristic':NoDataDependentFactor()}

dataHandler = DataHandler(views, normalize = False) #determine views to use
datasets = dataHandler.getLigandsDatasets() #determine datasets, dataset cpu(20) does not work
sorted_datasets = dataHandler.orderDatasetsBySize(datasets)
sorted_datasets.remove('Q9Y5Y6')
sorted_datasets.remove('P00734') #gives singular matrix error
sorted_datasets.remove('P00742')
sorted_datasets.remove('P07477')
sorted_datasets = sorted_datasets[12:] #sorted_datasets[:12]

#sorted_datasets = list(reversed(sorted_datasets))

methods = [SingleRLSR(**params), ConcatRLSR(**params), CoRLSR(**params), SingleSVR(**params), ConcatSVR(**params), EpsRelCoSVR(**params), L2RelCoSVR(**params), SigmaCoSVR(**params)]

metrics = [RMSE()]
paramEvaluator = ParamEval(folds = 3, fracL_train = 0.5, fracU_train = 0.5)

experiment = Experiment(dataHandler, sorted_datasets, methods, metrics, paramEvaluator)
experiment.run(folds, L_ftrain = 0.3, U_ftrain = 0.7, U_ftest = 0.0, expID = "MACCSGPI")
