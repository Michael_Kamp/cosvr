'''
Created on 16.04.2015

@author: Michael Kamp
'''

import math
import numpy as np

class Metric:
    def __init__(self):
        self.identifier = "Generic Metric (please replace)"
    
    def __str__(self):
        return self.identifier
    
    def __call__(self, y_pred, y_true):
        return -1.0
    
    def __eq__(self, other):        
        return isinstance(other, self.__class__)
    
    def __hash__(self):
        return (hash(self.identifier) ^ hash(self.__class__))

class RMSE(Metric):
    def __init__(self):
        self.identifier = "RMSE"
    
    def calcRMSE(self, y_pred, y_true):
        if hasattr(y_pred, '__len__') and len(y_pred) == 0:
            return 0.0
        m = len(y_pred)
        RMSE = 1/float(math.sqrt(m))*np.linalg.norm(y_pred.reshape(-1)-y_true.reshape(-1))   
        return RMSE
    
    def __call__(self, y_pred, y_true):       
        if y_pred.ndim == 2:      
            RMSEs = []              
            for v in range(len(y_pred)): #for each view
                RMSEs.append(self.calcRMSE(y_pred[v], y_true))
            return np.array(RMSEs)
        else:
            return self.calcRMSE(y_pred, y_true)

class ScaledRMSE(Metric):
    def __init__(self):
        self.identifier = "scaledRMSE"
    
    def calcScaledRMSE(self, y_pred, y_true):
        if hasattr(y_pred, '__len__') and len(y_pred) == 0:
            return 0.0
        maxYvalue = np.max(np.abs(y_true)) #scaled rmse comparability of datasets
        if maxYvalue == 0: 
            print "maxYvalue", maxYvalue, "y_true", y_true 
            maxYvalue = 0.001
        m = len(y_pred)
        RMSE = 1/float(maxYvalue)*1/float(math.sqrt(m))*np.linalg.norm(y_pred.reshape(-1)-y_true.reshape(-1))   
        return RMSE
    
    def __call__(self, y_pred, y_true):       
        if y_pred.ndim == 2:      
            RMSEs = []              
            for v in range(len(y_pred)): #for each view
                RMSEs.append(self.calcScaledRMSE(y_pred[v], y_true))
            return np.array(RMSEs)
        else:
            return self.calcScaledRMSE(y_pred, y_true)
        
class MRE(Metric):
    def __init__(self):
        self.identifier = "MRE"
    
    def calcMRE(self, y_pred, y_true):
        if hasattr(y_pred, '__len__') and len(y_pred) == 0:
            return 0.0
        m = len(y_true)
        MRE = 0.0
        for i in xrange(m):
            if y_true[i] != 0.0:
                MRE += abs(y_true[i] - y_pred[i]) / y_true[i]
        MRE *= 1.0/float(m)
        return MRE
    
    def __call__(self, y_pred, y_true):       
        if y_pred.ndim == 2:      
            MREs = []              
            for v in range(len(y_pred)): #for each view
                MREs.append(self.calcMRE(y_pred[v], y_true))
            return np.array(MREs)
        else:
            return self.calcMRE(y_pred, y_true)
        
class SymetricMRE(Metric):
    def __init__(self):
        self.identifier = "symMRE"
    
    def calcMRE(self, y_pred, y_true):
        if hasattr(y_pred, '__len__') and len(y_pred) == 0:
            return 0.0
        m = len(y_true)
        MRE = np.sum(np.abs(y_true - y_pred)) / np.sum(np.abs(y_true) + np.abs(y_pred))
        MRE *= 2.0 / float(m)
        return MRE
    
    def __call__(self, y_pred, y_true):       
        if y_pred.ndim == 2:      
            MREs = []              
            for v in range(len(y_pred)): #for each view
                MREs.append(self.calcMRE(y_pred[v], y_true))
            return np.array(MREs)
        else:
            return self.calcMRE(y_pred, y_true)
        