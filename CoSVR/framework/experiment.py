'''
Created on 16.04.2015

@author: Michael Kamp
'''

from datetime import datetime
import gc
import sys, os, time
import numpy as np
from output import Output

#from output import Output
time_format = '%d.%m.%Y %H:%M:%S'
time_folder_format = '%Y-%m-%d %H-%M-%S'


class Experiment:
    def __init__(self, dataHandler, datasets, methods, metrics, paramEvaluator, metricForParamEval = None):
        self.dataHandler = dataHandler
        self.datasets = datasets
        self.methods = methods
        self.metrics = metrics
        self.paramEvaluator = paramEvaluator
        self.metricForParamEval = metricForParamEval
        if metricForParamEval == None:
            self.metricForParamEval = metrics[0]
        self.results = {}
        self.scores = {}
        self.scoreSTDs = {}
        self.durations = {}
        self.current_dir = os.path.dirname(os.path.abspath(sys.argv[0]))
        for method in self.methods:
            self.results[str(method)] = {}
            self.scores[str(method)] = {}
            self.scoreSTDs[str(method)] = {}
            self.durations[str(method)] = {}
            for dataset in datasets:
                self.results[str(method)][dataset] = {}
                self.durations[str(method)][dataset] = 0.0
                self.scores[str(method)][dataset] = {}
                self.scoreSTDs[str(method)][dataset] = {}
                for metric in metrics:
                    self.scoreSTDs[str(method)][dataset][metric] = {}
                    self.scoreSTDs[str(method)][dataset][metric]['mean'] = {}
                    self.scoreSTDs[str(method)][dataset][metric]['std'] = {}
                

    def run(self, folds,  L_ftrain = 0.1, U_ftrain = 0.8, U_ftest = 0.1, numberLabeledExamples = None, expID = "", doParamEval = True):
        #create experiment subfolder with timestamp
        startTime = self.expTimerStart()
        timeString = time.strftime(time_folder_format, time.localtime(startTime))
        print "Starting experiment at " + timeString
        folderPath = os.path.join(self.current_dir, timeString + "_" + expID)
        os.mkdir(folderPath)
        self.exp_dir = folderPath
        #loop over all datasets and methods and run experiments
        out = Output(self.exp_dir)
        out.logging_start(startTime, folds, self.methods)
        out.writeExperimentParameters(self.methods, self.metrics, self.datasets, self.dataHandler.views, self.dataHandler.bNormalize, folds, self.paramEvaluator.folds, doParamEval, L_ftrain, U_ftrain, U_ftest, expID)
        for dataset in self.datasets:
            data = self.dataHandler.getData(dataset)
            print "Dataset "+str(data)
            #for UCI data generate possible view combinations
            viewCombinations = [data.views]
            number_of_views = len(self.dataHandler.views) #the methods should use this many views
            print len(data.views)
            if len(data.views) == 0 and len(self.dataHandler.views) > 1: #the data has only one view (e.g., UCI data)
                #for UCI data generate possible view combinations
                viewCombinations = data.generateViewCombinations(number_of_views, folds)
            for iFold in xrange(folds):
                print "Fold "+str(iFold+1)+" method: ",
                if len(viewCombinations) > 1:
                    data.setViews(viewCombinations[iFold])
                #do train/test split
                L_train, U_train, U_test = data.getTrainTestSplit(data.getXidx(), L_ftrain, U_ftrain, U_ftest, numLfix = numberLabeledExamples)
                for method in self.methods:
                    print str(method) + ": ",
                    if doParamEval:
                        params = self.paramEvaluator.getBestParams(method, self.metricForParamEval, data, L_train, U_train)
                        method.setParams(params)
                        out.logging_params(method, dataset, iFold, params)
                    method.checkKernel = False
                    print "training...",
                    start = datetime.now()
                    method.train(L_train, U_train, data)
                    num_svs = method.getNumberOfSupportVectors()
                    print " number of support vectors: ",num_svs,
                    print "testing...",
                    y_testpred = method.predict(U_test, data)
                    y_trainpred = method.predict(U_train, data)
                    stop = datetime.now()
                    duration = (stop - start).total_seconds()
                    self.results[str(method)][dataset][iFold] = {}
                    self.results[str(method)][dataset][iFold]['true_train'] = data.getYU(U_train)
                    self.results[str(method)][dataset][iFold]['pred_train'] = y_trainpred
                    self.results[str(method)][dataset][iFold]['true_test']  = data.getYU(U_test)
                    self.results[str(method)][dataset][iFold]['pred_test']  = y_testpred
                    self.results[str(method)][dataset][iFold]['params']     = method.getParamsAsString()
                    self.results[str(method)][dataset][iFold]['numSVs']     = num_svs
                    self.durations[str(method)][dataset] += duration / float(folds)
                    for metric in self.metrics:
                        self.calcMetrics(metric, method, dataset, data, folds, iFold, y_testpred, y_trainpred, U_test, U_train)
                    print "done."
            gc.collect()
            out.logging_dataset(self.scores, dataset, self.methods, self.metrics, self.results, number_of_views)
            print "done."

        endTime = self.expTimerStop(startTime)
        print "Experiment finished at " + time.strftime(time_folder_format, time.localtime(endTime))
        out.logging_end(endTime)
        #out.drawDiagrams(self.results, self.scores, self.durations, self.dataHandler, self.datasets)
        out.writeExperimentSummary(self.scores, self.durations, self.dataHandler.datasetInfo, self.methods, self.metrics, startTime, endTime, folds, self.datasets)
        out.pickleResults(self.results, self.scores, self.durations, self.dataHandler, self.datasets, self.scoreSTDs)

    def calcMetrics(self, metric, method, dataset, data, folds, iFold, y_testpred, y_trainpred, U_test, U_train):
        iFold += 1 #folds are zero-based, we need 1-based values
        if metric not in self.scores[str(method)][dataset]:
            if y_testpred.ndim == 1 or isinstance(y_testpred, float):
                self.scores[str(method)][dataset][metric] = {'unlabeled_train':0.0,'unlabeled_test':0.0,'unlabeled_all':0.0}
                self.scoreSTDs[str(method)][dataset][metric]['mean'] = {'unlabeled_train':0.0,'unlabeled_test':0.0,'unlabeled_all':0.0}
                self.scoreSTDs[str(method)][dataset][metric]['std'] = {'unlabeled_train':0.0,'unlabeled_test':0.0,'unlabeled_all':0.0}                                 
            elif y_testpred.ndim == 2:
                self.scores[str(method)][dataset][metric] = {'unlabeled_train':np.zeros(len(y_testpred)),'unlabeled_test':np.zeros(len(y_testpred)),'unlabeled_all':np.zeros(len(y_testpred))}
                self.scoreSTDs[str(method)][dataset][metric]['mean'] = {'unlabeled_train':np.zeros(len(y_testpred)),'unlabeled_test':np.zeros(len(y_testpred)),'unlabeled_all':np.zeros(len(y_testpred))}
                self.scoreSTDs[str(method)][dataset][metric]['std'] = {'unlabeled_train':np.zeros(len(y_testpred)),'unlabeled_test':np.zeros(len(y_testpred)),'unlabeled_all':np.zeros(len(y_testpred))}
        self.scores[str(method)][dataset][metric]['unlabeled_test'] += 1/float(folds)*metric(y_testpred, data.getYU(U_test))
        self.scores[str(method)][dataset][metric]['unlabeled_train'] += 1/float(folds)*metric(y_trainpred, data.getYU(U_train))
        std_test_Xk = metric(y_testpred, data.getYU(U_test))
        std_train_Xk = metric(y_trainpred, data.getYU(U_train))
        std_test_Mk_1 = self.scoreSTDs[str(method)][dataset][metric]['mean']['unlabeled_test']
        std_train_Mk_1 = self.scoreSTDs[str(method)][dataset][metric]['mean']['unlabeled_train']
        std_test_Mk = std_test_Mk_1 + (std_test_Xk - std_test_Mk_1) / float(iFold)
        std_train_Mk = std_train_Mk_1 + (std_train_Xk - std_train_Mk_1) / float(iFold)
        std_test_Sk_1 = self.scoreSTDs[str(method)][dataset][metric]['std']['unlabeled_test']
        std_train_Sk_1 = self.scoreSTDs[str(method)][dataset][metric]['std']['unlabeled_train']
        std_test_Sk = std_test_Sk_1 + (std_test_Xk-std_test_Mk_1)*(std_test_Xk-std_test_Mk)
        std_train_Sk = std_train_Sk_1 + (std_train_Xk-std_train_Mk_1)*(std_train_Xk-std_train_Mk)
        self.scoreSTDs[str(method)][dataset][metric]['mean']['unlabeled_test']  = std_test_Mk
        self.scoreSTDs[str(method)][dataset][metric]['mean']['unlabeled_train'] = std_train_Mk
        self.scoreSTDs[str(method)][dataset][metric]['std']['unlabeled_test']   = std_test_Sk
        self.scoreSTDs[str(method)][dataset][metric]['std']['unlabeled_train']  = std_train_Sk
        y_pred = np.hstack((y_testpred, y_trainpred))
        ytest = data.getYU(U_test)
        ytrain = data.getYU(U_train)
        y = np.hstack((ytest, ytrain))
        self.scores[str(method)][dataset][metric]['unlabeled_all'] += 1/float(folds)*metric(y_pred, y)
        std_all_Xk = metric(y_pred, y)        
        std_all_Mk_1 = self.scoreSTDs[str(method)][dataset][metric]['mean']['unlabeled_all']        
        std_all_Mk = std_all_Mk_1 + (std_all_Xk - std_all_Mk_1) / float(iFold)        
        std_all_Sk_1 = self.scoreSTDs[str(method)][dataset][metric]['std']['unlabeled_all']        
        std_all_Sk = std_all_Sk_1 + (std_all_Xk-std_all_Mk_1)*(std_all_Xk-std_all_Mk)        
        self.scoreSTDs[str(method)][dataset][metric]['mean']['unlabeled_all'] = std_all_Mk
        self.scoreSTDs[str(method)][dataset][metric]['std']['unlabeled_all'] = std_all_Sk
        

    def expTimerStart(self):
        startTime = time.time()
        timestamp = time.strftime(time_format, time.localtime(startTime))
        print "Experiment started at %s" % (timestamp)
        return startTime

    def expTimerStop(self, start_time):
        endTime = time.time()
        print "Experiment ended at %s" % (time.strftime(time_format, time.localtime(endTime)))
        seconds = endTime - start_time
        minutes = int(seconds) / 60
        rest = int(seconds - minutes * 60)
        print "Duration: %d minutes and %d seconds" % ((int(minutes)), int(rest))
        return endTime
