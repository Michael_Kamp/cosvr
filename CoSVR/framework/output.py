'''
Created on 28.04.2015

@author: Michael Kamp
'''

import time
import pickle
import os

MAX_TABLE_ELEMENT_LENGTH = 32
RESULTS_DIR = "results/"

class Output:
    def __init__(self, outDir, timeFormat = '%d.%m.%Y %H:%M:%S'):
        self.outDir = outDir
        self.timeFormat = timeFormat
        
    def writeExperimentSummary(self, scores, durations, datasetInfo, methods, metrics, startTime, endTime, nFolds, datasets):
        stOut = "Experiment started at %s, ended at %s" % (time.strftime(self.timeFormat, time.localtime(startTime)), time.strftime(self.timeFormat, time.localtime(endTime)))+"\n\n"
        stOut += "Experiments conducted using "+str(nFolds)+"-fold cross-validation.\n\n"
        for metric in metrics:
            stOut += self.createTableForMetric(metric, scores, durations, datasetInfo, methods, datasets)
        f = open(self.outDir + "/summary.txt", "w")
        f.write(stOut)
        f.close()
     
    def logging_params(self, method, dataset, iFold, params):
        stOut = str(method) + "\t" + ("\t" if len(str(method)) < 7 else "") + str(dataset) + "\t" + str(iFold) + "\t" + str(params) + "\n"
        f = open(self.outDir + "/parameters.txt", "a")
        f.write(stOut)
        f.close()
    
    def logging_dataset(self, scores, dataset, methods, metrics, results, numViews):
        stOut = "%11s"%(dataset)
        #print "scores", scores
        for method in methods:
            if str(method) == 'SingleSVR' or str(method) == 'SingleRLSR':
                avgSVs = [0.0]*numViews
                for fold in results[str(method)][dataset].keys():
                    numSVs = results[str(method)][dataset][fold]['numSVs']
                    for i in xrange(numViews):
                        avgSVs[i] += numSVs[i]                    
                folds = float(len(results[str(method)][dataset].keys()))
                for i in xrange(numViews):
                    avgSVs[i] /= folds
                for i in xrange(numViews):
                    stOut += "%11.3f [%3.1f]" %(scores[str(method)][dataset][metrics[0]]['unlabeled_train'][i], avgSVs[i])
                    stOut += "/"
                stOut = stOut[:-1] #remove the last "/"
            else:
                avgSVs = 0.0
                for fold in results[str(method)][dataset].keys():
                    avgSVs += results[str(method)][dataset][fold]['numSVs']
                avgSVs /= float(len(results[str(method)][dataset].keys()))
                stOut += "%11.3f(%3.3f) [%3.1f]"%(scores[str(method)][dataset][metrics[0]]['unlabeled_train'], scores[str(method)][dataset][metrics[0]]['unlabeled_test'], avgSVs) #metrics[0] = 'RMSE'
        stOut += "\n"  
        f = open(self.outDir + "/logging.txt", "a")
        f.write(stOut)
        f.close()
    
    def logging_start(self, startTime, nFolds, methods):
        stOut = "Experiment started at %s " % (time.strftime(self.timeFormat, time.localtime(startTime)))+"\n\n"
        stOut += "Experiments conducted using "+str(nFolds)+"-fold cross-validation.\n\n"
        stOut += "-----------------------------\n"
        stOut += "%11s"%(" ")
        for method in methods:
            stOut += "%19s"%str(method)
        stOut += "\n\n"
        f = open(self.outDir + "/logging.txt", "w")
        f.write(stOut)
        f.close()
        
    def logging_end(self, endTime):
        stOut = "Experiment ended at %s " % (time.strftime(self.timeFormat, time.localtime(endTime)))+"\n\n"
        f = open(self.outDir + "/logging.txt", "a")
        f.write(stOut)
        f.close()   
    
    #def drawDiagrams(self, results, scores, durations, dataHandler, datasets):
        #outDir = self.outDir + "/" + RESULTS_DIR
        #if not os.path.exists(outDir):
        #    os.makedirs(outDir)
        #scores = pickle.load(open(outDir + 'scores.pck','r'))  
        #print "scores"
        #print scores
        #print "results"
        #print results
    
    def pickleResults(self, results, scores, durations, dataHandler, datasets, scoreSTDs):
        outDir = self.outDir + "/" + RESULTS_DIR
        if not os.path.exists(outDir):
            os.makedirs(outDir)
        dataInfo = self.getDataInfo(dataHandler, datasets)
        pickle.dump(results, open(outDir+"results.pck", 'w'))
        pickle.dump(scores, open(outDir+"scores.pck", 'w'))
        pickle.dump(scoreSTDs, open(outDir+"scoreSTDs.pck", 'w'))
        pickle.dump(durations, open(outDir+"durations.pck", 'w'))
        pickle.dump(dataInfo, open(outDir+"dataInfo.pck", 'w'))
        #durations = pickle.load(open('durations.pck','r'))        
    
    def getDataInfo(self, dataHandler, datasets):
        dataInfo = {}
        for dataset in datasets:
            data = dataHandler.getData(dataset)
            dataInfo[dataset] = {}
            dataInfo[dataset]['type'] = data.type
            dataInfo[dataset]['dim'] = data.getDimension()
            dataInfo[dataset]['views'] = data.views
            dataInfo[dataset]['instances'] = data.dataInfo['instances']
        return dataInfo
            
            
    def createTableForMetric(self, metric, scores, durations, datasetInfo, methods, datasets):
        """
        scores = {}
        scores[method] = {}
        scores[method][dataset] = {}
        scores[method][dataset][metric] = {'unlabelled_train' : ..., 'unlabelled_all' : ..., 'unlabelled_test' : ...}
        """
        stOut = "-------------" + str(metric) + "----------------\n"
        stOut += "%11s"%(" ")
        for method in methods:
            stOut += "%19s"%str(method)
        stOut += "\n"
        for dataset in datasets:
            stOut += "%11s"%(dataset)
            for method in methods:
                if isinstance(scores[str(method)][dataset][metric]['unlabeled_train'], float):
                    stOut += "%11.2f(%3.2f)"%(scores[str(method)][dataset][metric]['unlabeled_train'], scores[str(method)][dataset][metric]['unlabeled_test'])
                else:
                    stOut += str(scores[str(method)][dataset][metric]['unlabeled_train']) + "(" + str(scores[str(method)][dataset][metric]['unlabeled_test']) + ")"
            stOut += "\n"   
        """
        stOut = "".ljust(MAX_TABLE_ELEMENT_LENGTH) + "\t" + "".ljust(MAX_TABLE_ELEMENT_LENGTH)+ "\t" + "".ljust(MAX_TABLE_ELEMENT_LENGTH) + "\t"
        for method in methods:
            stOut += str(method)[0:2*MAX_TABLE_ELEMENT_LENGTH].ljust(6*MAX_TABLE_ELEMENT_LENGTH) + "\t"
        stOut += "\n" + "".ljust(MAX_TABLE_ELEMENT_LENGTH) + "\t" + "".ljust(MAX_TABLE_ELEMENT_LENGTH)+ "\t" + "".ljust(MAX_TABLE_ELEMENT_LENGTH) + "\t"
        for method in methods:
            stOut += str(metric)[0:MAX_TABLE_ELEMENT_LENGTH].ljust(MAX_TABLE_ELEMENT_LENGTH) + "\t" + "|" + "\t" + ("duration")[0:MAX_TABLE_ELEMENT_LENGTH].ljust(MAX_TABLE_ELEMENT_LENGTH) + "\t"
        stOut = stOut[:-2] + "\n"    
        for dataset in datasets:          
            dtype = None
            for t in datasetInfo:
                if dataset in datasetInfo[t]:
                    dtype = t   
            dataInfo = str(dataset)+"("+str(dtype)+")"[0:int(0.5*MAX_TABLE_ELEMENT_LENGTH)].ljust(int(0.5*MAX_TABLE_ELEMENT_LENGTH))
            stOut += dataInfo+"\t"
            for method in scores:                
                stOut += str(scores[str(method)][dataset][metric])[0:6*MAX_TABLE_ELEMENT_LENGTH].ljust(6*MAX_TABLE_ELEMENT_LENGTH)+"\t|\t"+str(durations[str(method)][dataset])[0:MAX_TABLE_ELEMENT_LENGTH].ljust(MAX_TABLE_ELEMENT_LENGTH)+"\t"
            stOut += "\n"
        """
        return stOut
    
    def writeExperimentParameters(self, methods, metrics, datasets, views, bNormalize, folds, paramEvalFolds, doParamEval, L_ftrain, U_ftrain, U_ftest, expID):
        stOut = "Experimental Setup (expID="+expID+"):\n\n"
        stOut += "Datasets (normalized="+str(bNormalize)+"):\t" + str(datasets)+"\n"
        stOut += "Views:\t" + str(views)+"\n"
        stOut += "Folds: "+str(folds) + ", folds for parameter evaluation: "+str(paramEvalFolds)+", do parameter evaluation = "+str(doParamEval)+"\n"
        stOut += "Splits: L_ftrain="+str(L_ftrain)+", U_ftrain="+str(U_ftrain)+", U_ftest="+str(U_ftest)+"\n"
        stOut += "Metrics: "+str(metrics)+"\n"
        stOut += "\n"
        stOut += "Methods:\n\n"
        for method in methods:
            stOut += str(method) + " with parameters " + method.getParamsAsString() + "\n"
            stOut += "\t parameter range: " + str(method.paramRange) + "\n"
            stOut += "-------------------------------------------------------------------------------------------------\n\n"
                
        f = open(self.outDir + "/setup.txt", "w")
        f.write(stOut)
        f.close()