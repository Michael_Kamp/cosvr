'''
Created on 05.05.2015

@author: Michael Kamp
'''

from datetime import datetime
import sys
import numpy as np
import copy

class ParamEval():
    def __init__(self, folds = 5, fracL_train = 0.5, fracU_train = 0.5):
    #def __init__(self, folds = 5, fracL_train = 0.9, fracU_train = 1.0):
        self.fracL_train = fracL_train
        self.fracU_train = fracU_train
        self.folds = folds    
        
    def getBestParams(self, method, metric, data, L, U):
        folds = self.folds
        print "Starting parameter evaluation... Number of folds: ",folds,", with ",len(data.views)," views",
        start = datetime.now()
        bestPerf = sys.float_info.max
        possParams = method.getPossParams(len(data.views))
        print ", number of parameters to check: ",len(possParams),". Go",
        bestParams = None  
        count = 0
        noModelCount = 0 
        for params in possParams:
            params = self.getDataDependentParams(method, params, data)      
            method.setParams(params)
            perf = 0.0
            for _ in xrange(folds): #splits for fold generated randomly
                L_train, U_train, U_test = self.getSplit(L, U)
                y_testpred = np.ones(data.getY(U_test).shape)*100.0
                try:
                    bSuccess = method.train(L_train, U_train, data)
                    if not bSuccess:
                        noModelCount += 1                    
                    y_testpred = method.predict(U_test, data)
                except:
                    noModelCount +=1
                perf += metric(y_testpred, data.getY(U_test)) / float(folds)
            if hasattr(perf, '__len__') or hasattr(perf, 'shape'): #SingleSVR returns an array of predictions, accordingly, the metric returns an array of error values. For parameter evaluation it suffices to optimizes with respect to the average of the error values
                perf = np.average(perf)
            if perf < bestPerf:
                bestPerf = perf
                bestParams = params
            if count % 100 == 0:
                print ".",
            count += 1
        stop = datetime.now()
        duration = (stop - start).total_seconds()
        print "For ",noModelCount, " out of ",count," param combinations no valid model could be computed."
        print "Best params found in time ",duration," with performance ", bestPerf, " for metric ",metric,". Params are: ",bestParams,"."        
        return bestParams
    
    def getSplit(self, L, U):
        Lidx = copy.deepcopy(L)  
        Uidx = copy.deepcopy(U)
        np.random.shuffle(Lidx)
        np.random.shuffle(Uidx)
        nl = len(Lidx)
        nu = len(Uidx)
        nltrain = int(self.fracL_train*nl)
        if nltrain == nl:
            nltrain -= 1
        nutrain = int(self.fracU_train*nu)
        L_train = Lidx[:nltrain]
        U_train = Uidx[:nutrain]
        U_test  = Lidx[nltrain:]
        return L_train, U_train, U_test
    
    def getDataDependentParams(self, method, orig_params, data):
        params = {}
        for name in orig_params:
            val = orig_params[name]
            if hasattr(val, '__len__'):
                params[name] = []
                for vidx in xrange(len(val)):
                    fact = method.paramValDataDependentFactor(name, data, vidx)
                    if fact != None and name != 'k':
                        params[name].append(orig_params[name][vidx]*fact)
                    else:
                        params[name].append(orig_params[name][vidx])
            else:
                fact = method.paramValDataDependentFactor(name, data)
                if fact != None and name != 'k':
                    params[name] = orig_params[name]*fact
                else:
                    params[name] = orig_params[name]
        return params
